<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Assignmentgrade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'assignment_grade';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'assignmentID', 'studentID', 'studentMarks', 'assignmentDate', 'addedBy'
    ];
}