<?php
#Created By sohail
#date 12/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Helpdesk extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'helpdesk';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'id',	'sendBy',	'sendTo',	'senderName',	'receiverName',	'description',	'helpID'
    ];
}