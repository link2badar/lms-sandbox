<?php
#Created By sohail
#date 12/8/2020
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Instructor extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'instructor';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'instructorfName', 'instructormName' ,'instructorlName', 'email', 'instructorPass', 'instructorPhone', 'instructorAddress','addedBy'
    ];
}