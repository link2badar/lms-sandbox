<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Submitexam extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'submit_exam';
    
    protected $primarykey='id';
    
    protected $fillable = [
         'description', 'file', 'examID', 'submittedBy'
    ];
}