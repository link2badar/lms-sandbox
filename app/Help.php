<?php
#Created By sohail
#date 12/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Help extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'help';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'subject','description','submitTo', 'addedBy'
    ];
}