<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Lecturegrade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lecture_grade';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'lectureID', 'studentID', 'studentMarks', 'lectureDate', 'addedBy'
    ];
}