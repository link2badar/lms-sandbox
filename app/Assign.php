<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Assign extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'assign';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'teacherID', 'courseID', 'studentID', 'addedBy', 'is_group'
    ];
}