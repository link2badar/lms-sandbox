<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Materialgrade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'material_grade';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'materialID', 'studentID', 'studentMarks', 'materialDate', 'addedBy'
    ];
}