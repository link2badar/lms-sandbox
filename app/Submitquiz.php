<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Submitquiz extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'submit_quiz';
    
    protected $primarykey='id';
    
    protected $fillable = [
         'description', 'file', 'quizID', 'submittedBy'
    ];
}