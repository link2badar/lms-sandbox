<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Lecture extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'lecture';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'title', 'description', 'file', 'courseID', 'addedBy'
    ];
}