<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Material extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'material';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'title', 'description', 'file', 'courseID', 'addedBy'
    ];
}