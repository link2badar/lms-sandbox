<?php
#Created By Sohail
#date 19/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Assignment;
use App\Submitassignment;
use App\Assignmentgrade;
use App\Instructor;
use App\Assign;
use App\Student;
use App\Quiz;
use App\Quizgrade;
use App\Submitquiz;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use ZipArchive;
use DB;


class QuizController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Quizzes';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Quizzes');
        $data['result']         =   Quiz::orderBy('quiz.id', 'DESC')->join('courses', 'courses.id', '=', 'quiz.courseID')->select('quiz.*', 'courses.courseName')->where('quiz.addedBy', Auth::user()->id)->get()->toArray();
        $data['gradeFilter']    =   Quizgrade::where('addedBy', Auth::user()->id)->select('quizID')->get()->toArray();
        return view('instructor.quiz.all_quizzes', $data);
    }

    public function uploadQuiz(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.quiz.add_new_quiz', $data);
    }
    
    public function saveQuiz(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description',
            'deadline' => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $currentTime            =   strtotime(date("H:i"));
        $inputTime              =   strtotime($data['deadline']);
        $resultTime             =   date("H:i",$currentTime+$inputTime);
        $data['deadline']       =   date("Y-m-d H:i", strtotime(date("Y-m-d").' '.$resultTime));
        Quiz::create($data);
        Session::flash('message', 'Quiz has been delivered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function download($archive_file_name)
    {
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile("$archive_file_name");
    }

    public function getStudentAssignments($id)
    {
        $data['result']         =   Assignment::orderBy('assignment.id', 'DESC')->join('courses', 'courses.id', '=', 'assignment.courseID')->select('assignment.*', 'courses.courseName')->where('assignment.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'All Assignments');
        $data['submissionStatus']=  Submitassignment::where('submittedBy', Auth::user()->id)->get()->toArray();
        return view('student.all_assignments', $data);
    }

    public function assignmentSubmit($id)
    {
        $data['id']         =       $id;
        $data['result']     =       Assignment::where('id', $id)->get()->toArray();
        return view('student.modal.assignment_submit', $data);
    }

    public function submitAssignmentByStudent(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Submitassignment::create($data);
        Session::flash('message', 'assignment has been uploaded successfully!'); 
        return Response()->json(['success' => '1']);
        
    }

    public function submissionList($id)
    {
        $data['result']         =   Submitquiz::orderBy('submit_quiz.id', 'DESC')->join('quiz', 'quiz.id', '=', 'submit_quiz.quizID')->join('users', 'users.id', '=', 'submit_quiz.submittedBy')->select('submit_quiz.*', 'quiz.title', 'users.name', 'quiz.deadline')->where('submit_quiz.quizID', $id)->get()->toArray();
        $data['pageName']       =   'Quiz';
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-quizzes" => 'All Quizzes', '#' => '');
        if(count($data['result'])>0){
            $data['pageName']       =   'Quiz: '.$data['result'][0]['title'];
            $data['breadcrumbs']    =   array("dashboard" => "Home", "all-quizzes" => 'All Quizzes', '#' => $data['result'][0]['title']);
        }
        return view('instructor.quiz.all_submitted_quizzes', $data);
    }

    public function quizGrade($id)
    {
        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('quiz.id' => $id, 'assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->join('quiz', 'quiz.courseID', '=', 'assign.courseID')->select('quiz.id', 'assign.studentID', 'quiz.title', 'quiz.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('quiz.id' => $id, 'assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->join('quiz', 'quiz.courseID', '=', 'assign.courseID')->select('quiz.id', 'assign.studentID', 'quiz.title', 'quiz.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"), DB::raw("GROUP_CONCAT(student.id) as sID"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        return view('instructor.quiz.add_grade', $data);
    }
    
    public function saveQuizGrade(Request $request)
    {
        $data = $request->all();
        $data['studentID']          =   implode(",", $data['studentID']);
        $data['studentMarks']       =   implode(",", $data['studentMarks']);
        $data['quizDate']           =   date("Y-m-d", strtotime($data['quizDate']));
        Quizgrade::create($data);
        Session::flash('message', 'Grades has been submitted successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function allAssignmentGrade()
    {
        $data['pageName']       =   'All Assignment Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment Grade');
        $data['result']         =   Assignmentgrade::where('assignment_grade.addedBy', Auth::user()->id)->select('assignment_grade.*', 'assignment.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"))->join('assignment', 'assignment.id', '=', 'assignment_grade.assignmentID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, assignment_grade.studentID)"),">",DB::raw("'0'"))->groupBy("assignment_grade.id")->get()->toArray();
        return view('instructor.assignment.all_assignments_grade', $data);
        
    }

    public function studentAssignmentGrade()
    {
        $data['pageName']       =   'All Assignment Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment Grade');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Assignmentgrade::whereRaw("find_in_set(".$studentID[0]['id'].",assignment_grade.studentID)")->select('assignment_grade.*', 'assignment.title')->join('assignment', 'assignment.id', '=', 'assignment_grade.assignmentID')->get()->toArray();
        return view('student.grade.assignment', $data);
        
    }


    public function getStudentQuizzes($id)
    {
        $data['result']         =   Quiz::orderBy('quiz.id', 'DESC')->join('courses', 'courses.id', '=', 'quiz.courseID')->select('quiz.*', 'courses.courseName')->where('quiz.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'All Quizzes');
        $data['submissionStatus']=  Submitquiz::where('submittedBy', Auth::user()->id)->get()->toArray();
        return view('student.all_quizzes', $data);
    }
    public function quizSubmit($id)
    {
        $data['id']         =       $id;
        $data['result']     =       Quiz::where('id', $id)->get()->toArray();
        return view('student.modal.quiz_submit', $data);
    }

    public function submitQuizByStudent(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Submitquiz::create($data);
        Session::flash('message', 'Quiz has been uploaded successfully!'); 
        return Response()->json(['success' => '1']);
        
    }

    public function studentQuizGrade()
    {
        $data['pageName']       =   'All Quiz Grades';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Quiz Grades');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Quizgrade::whereRaw("find_in_set(".$studentID[0]['id'].",quiz_grade.studentID)")->select('quiz_grade.*', 'quiz.title')->join('quiz', 'quiz.id', '=', 'quiz_grade.quizID')->get()->toArray();
        return view('student.grade.quiz', $data);
    }

    public function allQuizGrade()
    {
        $data['pageName']       =   'All Quiz Grades';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Quiz Grades');
        $data['result']         =   Quizgrade::where('quiz_grade.addedBy', Auth::user()->id)->select('quiz_grade.*', 'quiz.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(classes.className) as className") , DB::raw("GROUP_CONCAT(student.email) as studentEmail"))->join('quiz', 'quiz.id', '=', 'quiz_grade.quizID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, quiz_grade.studentID)"),">",DB::raw("'0'"))->join('classes', 'classes.id', '=', 'student.class_id')->groupBy("quiz_grade.id")->get()->toArray();
        return view('instructor.quiz.all_quiz_grade', $data);
        
    }
    
}