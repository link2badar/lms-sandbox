<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Help;
use App\Chat;
use App\Student;    
use App\Classes;
use App\Instructor;
use App\Assign;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use DB;


class ChatController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     //get all issues raised by student 
    public function studentChat($id = null)
    {
        $data['pageName']       =   'Chat Box';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Chat Box');
        $studentAuthID          =   Student::where('email', Auth::user()->email)->select('id', 'class_id')->get()->toArray();
        
        $data['instructor']     =   Assign::orderBy('assign.id', 'DESC')->join('instructor','instructor.id', '=', 'assign.teacherID')->groupBy('instructor.id')->select('instructor.id', 'instructor.instructorfName', 'instructor.instructormName', 'instructor.instructorlName')->whereRaw("find_in_set(".$studentAuthID[0]['id'].",assign.studentID)")->where('assign.is_group', 0)->get()->toArray();

        $data['instructorByGroup']    =   Assign::orderBy('assign.id', 'DESC')->join('instructor','instructor.id', '=', 'assign.teacherID')->groupBy('instructor.id')->select('instructor.id', 'instructor.instructorfName', 'instructor.instructormName', 'instructor.instructorlName')->whereRaw("find_in_set(".$studentAuthID[0]['class_id'].",assign.studentID)")->where('assign.is_group', 1)->get()->toArray();

        $data['result']         =   '';
        if($id != null){
            $data['result']         =   Chat::where(function ($query) use ($id)  {
                $query->where('sendBy', Auth::user()->id)
                    ->where('sendTo', $id);
            })->orWhere(function($query) use ($id) {
                $query->where('sendTo', Auth::user()->id)
                    ->where('sendBy',$id);	
            })->get()->toArray();
            $data['instructorName'] =   Instructor::where('id', $id)->select('instructor.id', 'instructor.instructorfName', 'instructor.instructormName', 'instructor.instructorlName')->get()->toArray();
        }
        
        return view('student.chat.allchats', $data);
    }


    public function instructorChat($id = null)
    {
        $data['pageName']       =   'Chat Box';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Chat Box');

        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->select(DB::raw("GROUP_CONCAT(users.id) as studentID"), DB::raw("GROUP_CONCAT(student.studentfName) as studentfName"), DB::raw("GROUP_CONCAT(student.studentlName) as studentlName"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))->join('users', 'users.email', '=', 'student.email')
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->select(DB::raw("GROUP_CONCAT(users.id) as sID"),  DB::raw("GROUP_CONCAT(student.studentfName) as studentfName"), DB::raw("GROUP_CONCAT(student.studentlName) as studentlName"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))->join('users', 'users.email', '=', 'student.email')
        ->groupBy("assign.id")->get()->toArray();


        $data['result']         =   '';
        if($id != null){
            $data['result']         =   Chat::where(array('sendBy' => Auth::user()->id, 'sendTo' => $id))->orWhere(array('sendTo' => Auth::user()->id, 'sendBy' => $id))->get()->toArray();

            $data['studentName'] =   User::where('id', $id)->select('id', 'name')->get()->toArray();
        }
        
        return view('instructor.chat.allchats', $data);
    }


    public function schoolHelp()
    {
        $data['pageName']       =   'Help Desk';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Desk');
        $data['result']         =   Help::orderBy('id', 'DESC')
        ->join('users', 'users.id', '=', 'help.addedBy')
        ->join('student', 'student.email', '=', 'users.email')
        ->join('classes', 'classes.id', '=', 'student.class_id')
        ->select('help.*', 'users.name', 'users.email', 'classes.className')
        ->where('help.submitTo', Auth::user()->id)
        ->get()->toArray();
        return view('schooladmin.help.allhelps', $data);
    }
    
    public function registerStudentHelp()
    {
        return view('student.help.add_new_help');
    }

    public function submitStudentHelp(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'subject'       =>  'required',
            'description'   =>  'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $submitTo       =   Student::where('email', Auth::user()->email)->select('addedBy')->get()->toArray();
        $data           =   $request->all();
        $data['submitTo']   =   $submitTo[0]['addedBy'];


        Help::create($data);
        Session::flash('message', 'Issue has been Submitted To school Admin successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function helpDetail($id)
    {
        $data['pageName']       =   'Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('student.help.help_detail', $data);
    }

    public function schoolHelpDetail($id)
    {
        $data['pageName']       =   'Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.addedBy')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('schooladmin.help.help_detail', $data);
    }

    public function schoolHelpIntervalChat($id)
    {
        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.addedBy')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return Response()->json($data);
    }
    
    public function chatIntervalChat($id)
    {
        $data['helpData']       =   Chat::where(array('sendBy' => Auth::user()->id, 'sendTo' => $id))->orWhere(array('sendTo' => Auth::user()->id, 'sendBy' => $id))->get()->toArray();
        return Response()->json($data);
    }

    public function instructorChatIntervalChat($id)
    {
        $data['helpData']       =   Chat::where(array('sendBy' => Auth::user()->id, 'sendTo' => $id))->orWhere(array('sendTo' => Auth::user()->id, 'sendBy' => $id))->get()->toArray();
        return Response()->json($data);
    }

    public function saveChatDetail(Request $request)
    {
        $data           =   $request->all();
        Chat::create($data);
        $returnData['senderName']       =   $data['senderName'];
        $returnData['senderDate']       =   date("d/M/y h:i");
        $returnData['description']      =   $data['description'];
        return Response()->json($returnData);
        
    }

    public function instructorSaveChatDetail(Request $request)
    {
        $data           =   $request->all();
        Chat::create($data);
        $returnData['senderName']       =   $data['senderName'];
        $returnData['senderDate']       =   date("d/M/y h:i");
        $returnData['description']      =   $data['description'];
        return Response()->json($returnData);
        
    }
}