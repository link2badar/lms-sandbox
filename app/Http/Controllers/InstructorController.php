<?php
#Created By sohail
#date 12/8/2020

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Instructor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class InstructorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Instructors';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Instructors');
        $data['result']         =   Instructor::orderBy('id', 'DESC')->where('addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.allinstructors', $data);
    }

    public function add(Request $request)
    {

        return view('schooladmin.modals.instructor.add_new_instructor');
    }
    
    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' =>  'required|email|unique:instructor|unique:users',
            'instructorfName'  =>  'required',
            'instructorlName'  =>  'required',
            'instructorPass'  =>  'required|min:6',
            'instructorPhone'  =>  'required',
            'instructorAddress'  =>  'required',
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        
        $data = $request->all();
        $data['instructorPass'] =  Hash::make($data['instructorPass']);
        Instructor::create($data);
        
        $user   =   new User();
        $user->name = $data['instructorfName']." ".$data['instructorlName'];
        $user->email = $data['email'];

        $data = $request->all();
        $user->password = Hash::make($data['instructorPass']);
        $user->role     =   2;
        $user->save();

        Session::flash('message', 'Instructor has been registered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $user     =   Instructor::where('id', $id)->get()->toArray();
        User::where('email', $user[0]['email'])->delete();
        $data                   =   Instructor::where('id', $id)->delete();
        Session::flash('message', 'Instructor has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Instructor::where('id', $id)->get()->toArray();
        return view('schooladmin.modals.instructor.editinstructor', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $school = Instructor::findOrFail($id);
        $upass = $input['instructorPass'];
        if($input['instructorPass'] != '')
            $input['instructorPass'] = Hash::make($input['instructorPass']);
        else
            unset($input['instructorPass']);

            $validator = \Validator::make($request->all(), [
            'instructorfName'  =>  'required',
            'instructorlName'  =>  'required',
            'instructorPhone'  =>  'required',
            'instructorAddress'  =>  'required',
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $school->fill($input)->save();

        $user = User::where('email', $input['email'])->first();
        $user->name = $input['instructorfName']." ".$input['instructorlName'];
        if( $upass != ''){
        $user->password = Hash::make($upass);
        }
        $user->save();
        Session::flash('message', $input['instructorfName']." ".$input['instructorlName'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function detail($id)
    {
        $data['result']         =   Instructor::where('id', $id)->get()->toArray();
        return view('schooladmin.modals.instructor.instructordetail', $data);
    }
}