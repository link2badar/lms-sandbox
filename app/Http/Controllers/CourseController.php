<?php
#Created By sohail
#date 12/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class CourseController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Courses';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Courses');
        $data['result']         =   Course::orderBy('id', 'DESC')->where('addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.allcourses', $data);
    }

    public function add(Request $request)
    {
        return view('schooladmin.modals.course.add_new_course');
    }
    
    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'courseName'  =>  'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        Course::create($data);
        Session::flash('message', 'Course has been registered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Course::where('id', $id)->delete();
        Session::flash('message', 'Course has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Course::where('id', $id)->get()->toArray();
        return view('schooladmin.modals.course.editcourse', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $Course = Course::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'courseName'  =>  'required'    
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $Course->fill($input)->save();

        Session::flash('message', $input['courseName'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }
}