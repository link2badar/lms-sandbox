<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Classes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class ClassController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Groups';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Groups');
        $data['result']         =   Classes::orderBy('id', 'DESC')->where('addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.allclasses', $data);
    }

    public function add(Request $request)
    {
        return view('schooladmin.modals.add_new_class');
    }
    
    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'className'  =>  'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        Classes::create($data);
        Session::flash('message', 'Group has been registered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Classes::where('id', $id)->delete();
        Session::flash('message', 'Group has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Classes::where('id', $id)->get()->toArray();
        return view('schooladmin.modals.editclass', $data);
    }

    public function addSub($id)
    {

        $data['parent']     =   $id;
        return view('schooladmin.modals.subgroup.addsub', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $classes = Classes::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'className'  =>  'required'    
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $classes->fill($input)->save();

        Session::flash('message', $input['className'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }
}