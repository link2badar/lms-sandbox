<?php
#Created By Sohail
#date 19/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Assignment;
use App\Submitassignment;
use App\Assignmentgrade;
use App\Instructor;
use App\Assign;
use App\Student;
use App\Video;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use ZipArchive;
use DB;


class VideoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'Video Lectures';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Video Lectures');
        $data['result']         =   Video::orderBy('videos.id', 'DESC')->join('courses', 'courses.id', '=', 'videos.courseID')->select('videos.*', 'courses.courseName')->where('videos.addedBy', Auth::user()->id)->get()->toArray();
        return view('instructor.video.all_videos', $data);
    }



    public function uploadVideo(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.video.add_new_video', $data);
    }
    
    public function savevideo(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
         if ($request->hasFile('file')) {

             $file = $request->file('file');
             $destinationPath = public_path('/uploadedFile/'); // upload path
                $data['file'] = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
                $file->move($destinationPath, $data['file']);

        }

        Video::create($data);
        Session::flash('message', 'Video has been uploaded successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Video::where('id', $id)->delete();
        Session::flash('message', 'Video has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Video::where('id', $id)->get()->toArray();
        return view('instructor.video.edit_video', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        $assignment = Video::findOrFail($id);
        if ($request->hasFile('file')) {

             $file = $request->file('file');
             $destinationPath = public_path('/uploadedFile/'); // upload path
                $data['file'] = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
                $file->move($destinationPath, $data['file']);

        }
        $assignment->fill($data)->save();
        Session::flash('message', 'Video: '.$data['title'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function watch($archive_file_name)
    {
        $data = array();
        $data['pageName']       =   'Watch Video';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Watch Video');
        $data['file'] = $archive_file_name;

        return view('instructor.video.watch', $data);
    }

    public function getStudentVideos($id)
    {
        $data['result']         =   Video::orderBy('videos.id', 'DESC')->join('courses', 'courses.id', '=', 'videos.courseID')->select('videos.*', 'courses.courseName')->where('videos.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'All Videos');
        return view('student.all_videos', $data);
    }




}