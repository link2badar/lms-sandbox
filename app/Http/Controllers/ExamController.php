<?php
#Created By Sohail
#date 19/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Assignment;
use App\Submitassignment;
use App\Assignmentgrade;
use App\Instructor;
use App\Assign;
use App\Student;
use App\Exam;
use App\Examgrade;
use App\Submitexam;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use ZipArchive;
use DB;


class ExamController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Exams';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Exams');
        $data['result']         =   exam::orderBy('exam.id', 'DESC')->join('courses', 'courses.id', '=', 'exam.courseID')->select('exam.*', 'courses.courseName')->where('exam.addedBy', Auth::user()->id)->get()->toArray();
        $data['gradeFilter']    =   examgrade::where('addedBy', Auth::user()->id)->select('examID')->get()->toArray();
        return view('instructor.exam.all_exams', $data);
    }

    public function uploadExam(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.exam.add_new_exam', $data);
    }
    
    public function saveExam(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description',
            'deadline' => 'required',
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $currentTime            =   strtotime(date("H:i"));
        $inputTime              =   strtotime($data['deadline']);
        $resultTime             =   date("H:i",$currentTime+$inputTime);
        $data['deadline']       =   date("Y-m-d H:i", strtotime(date("Y-m-d").' '.$resultTime));
        Exam::create($data);
        Session::flash('message', 'Exam has been delivered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function download($archive_file_name)
    {
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile("$archive_file_name");
    }

    
    public function submissionList($id)
    {
        $data['result']         =   Submitexam::orderBy('submit_exam.id', 'DESC')->join('exam', 'exam.id', '=', 'submit_exam.examID')->join('users', 'users.id', '=', 'submit_exam.submittedBy')->select('submit_exam.*', 'exam.title', 'users.name', 'exam.deadline')->where('submit_exam.examID', $id)->get()->toArray();
        $data['pageName']       =   'Exam';
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-exams" => 'All Exams', '#' => '');
        if(count($data['result'])>0){
            $data['pageName']       =   'Exam: '.$data['result'][0]['title'];
            $data['breadcrumbs']    =   array("dashboard" => "Home", "all-exams" => 'All Exams', '#' => $data['result'][0]['title']);
        }
        return view('instructor.exam.all_submitted_exams', $data);
    }

    public function examGrade($id)
    {
        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('exam.id' => $id, 'assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->join('exam', 'exam.courseID', '=', 'assign.courseID')->select('exam.id', 'assign.studentID', 'exam.title', 'exam.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('exam.id' => $id, 'assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->join('exam', 'exam.courseID', '=', 'assign.courseID')->select('exam.id', 'assign.studentID', 'exam.title', 'exam.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"), DB::raw("GROUP_CONCAT(student.id) as sID"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        return view('instructor.exam.add_grade', $data);
    }
    
    public function saveExamGrade(Request $request)
    {
        $data = $request->all();
        $data['studentID']          =   implode(",", $data['studentID']);
        $data['studentMarks']       =   implode(",", $data['studentMarks']);
        $data['examDate']           =   date("Y-m-d", strtotime($data['examDate']));
        Examgrade::create($data);
        Session::flash('message', 'Grades has been submitted successfully!'); 
        return Response()->json(['success' => '1']);
    }


    public function getStudentExams($id)
    {
    
        $data['result']         =   exam::orderBy('exam.id', 'DESC')->join('courses', 'courses.id', '=', 'exam.courseID')->select('exam.*', 'courses.courseName')->where('exam.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'All exams');
        $data['submissionStatus']=  Submitexam::where('submittedBy', Auth::user()->id)->get()->toArray();
        return view('student.all_exams', $data);
    }
    public function examSubmit($id)
    {
        $data['id']         =       $id;
        $data['result']     =       exam::where('id', $id)->get()->toArray();
        return view('student.modal.exam_submit', $data);
    }

    public function submitExamByStudent(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Submitexam::create($data);
        Session::flash('message', 'exam has been uploaded successfully!'); 
        return Response()->json(['success' => '1']);
        
    }

    public function studentExamGrade()
    {
        $data['pageName']       =   'All Exam Grades';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Exam Grades');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Examgrade::whereRaw("find_in_set(".$studentID[0]['id'].",exam_grade.studentID)")->select('exam_grade.*', 'exam.title')->join('exam', 'exam.id', '=', 'exam_grade.examID')->get()->toArray();
        return view('student.grade.exam', $data);
    }

    public function allExamGrade()
    {
        $data['pageName']       =   'All Exam Grades';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Exam Grades');
        $data['result']         =   Examgrade::where('exam_grade.addedBy', Auth::user()->id)->select('exam_grade.*', 'exam.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(classes.className) as className") , DB::raw("GROUP_CONCAT(student.email) as studentEmail"))->join('exam', 'exam.id', '=', 'exam_grade.examID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, exam_grade.studentID)"),">",DB::raw("'0'"))->join('classes', 'classes.id', '=', 'student.class_id')->groupBy("exam_grade.id")->get()->toArray();
        return view('instructor.exam.all_exam_grade', $data);
        
    }
    
}