<?php
#Created By Sohail
#date 19/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Material;
use App\Materialgrade;
use App\Instructor;
use App\Assign;
use App\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use ZipArchive;
use DB;


class MaterialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'Practice Material';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Practice Material');
        $data['result']         =   Material::orderBy('material.id', 'DESC')->join('courses', 'courses.id', '=', 'material.courseID')->select('material.*', 'courses.courseName')->where('material.addedBy', Auth::user()->id)->get()->toArray();
        $data['gradeFilter']    =   Materialgrade::where('addedBy', Auth::user()->id)->select('materialID')->get()->toArray();
        return view('instructor.material.all_materials', $data);
    }

    public function schoolindex()
    {
        $data['pageName']       =   'All Materials';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Materials');
        
        $data['result']         =   Material::orderBy('material.id', 'DESC')
        ->select('material.*', 'courses.courseName', 'users.name')
        ->join('courses', 'courses.id', '=', 'material.courseID')
        ->join('users', 'users.id', 'material.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->get()->toArray();
        
        return view('schooladmin.content.materials', $data);
    }

    public function uploadMaterial(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.material.add_new_material', $data);
    }
    
    public function saveMaterial(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Material::create($data);
        Session::flash('message', 'Material has been delivered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Material::where('id', $id)->delete();
        Session::flash('message', 'Material has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Material::where('id', $id)->get()->toArray();
        return view('instructor.Material.edit_Material', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        $Material = Material::findOrFail($id);
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $Material->fill($data)->save();
        Session::flash('message', 'Material: '.$data['title'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function download($archive_file_name)
    {
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile("$archive_file_name");
    }

    public function getStudentPractices($id)
    {
        $data['result']         =   Material::orderBy('material.id', 'DESC')->join('courses', 'courses.id', '=', 'material.courseID')->select('material.*', 'courses.courseName')->where('material.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'Practice Material');
        return view('student.all_materials', $data);
    }

    public function materialGrade($id)
    {
        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('material.id' => $id, 'assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->join('material', 'material.courseID', '=', 'assign.courseID')->select('material.id', 'assign.studentID', 'material.title', 'material.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('material.id' => $id, 'assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->join('material', 'material.courseID', '=', 'assign.courseID')->select('material.id', 'assign.studentID', 'material.title', 'material.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"), DB::raw("GROUP_CONCAT(student.id) as sID"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();
        
       
        return view('instructor.material.add_grade', $data);
    }
    
    public function saveMaterialGrade(Request $request)
    {
        $data = $request->all();
        $data['studentID']          =   implode(",", $data['studentID']);
        $data['studentMarks']       =   implode(",", $data['studentMarks']);
        Materialgrade::create($data);
        Session::flash('message', 'Grades has been submitted successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function allMaterialGrade()
    {
        $data['pageName']       =   'All Material Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Material Grade');
        $data['result']         =   Materialgrade::orderBy('material_grade.id', 'DESC')->where('material_grade.addedBy', Auth::user()->id)->select('material_grade.*', 'material.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(classes.className) as className") , DB::raw("GROUP_CONCAT(student.email) as studentEmail"))->join('material', 'material.id', '=', 'material_grade.materialID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, material_grade.studentID)"),">",DB::raw("'0'"))->groupBy("material_grade.id")->join('classes', 'classes.id', '=', 'student.class_id')->get()->toArray();
        return view('instructor.material.all_material_grade', $data);
        
    }

    public function studentMaterialGrade()
    {
        $data['pageName']       =   'All Material Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Material Grade');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Materialgrade::whereRaw("find_in_set(".$studentID[0]['id'].",material_grade.studentID)")->select('material_grade.*', 'material.title')->join('material', 'material.id', '=', 'material_grade.materialID')->get()->toArray();
        return view('student.grade.material', $data);
        
    }

    public function schoolMaterialGrade()
    {
        
        $data['pageName']       =   'All Material Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Material Grade');
        
        $data['result']         =   Materialgrade::
        select('material_grade.*', 'material.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), 'users.name')
        ->join('material', 'material.id', '=', 'material_grade.materialID')
        ->leftjoin("student",DB::raw("FIND_IN_SET(student.id, material_grade.studentID)"),">",DB::raw("'0'"))
        ->join('users', 'users.id', '=', 'material_grade.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->groupBy("material_grade.id")
        ->get()->toArray();

        return view('schooladmin.grade.material', $data);
        
    }
}