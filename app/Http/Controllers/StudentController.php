<?php
#Created By sohail
#date 14/8/2020

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class StudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Students';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Students');
        $data['result']         =   Student::orderBy('student.id', 'DESC')->join('classes', 'classes.id', '=', 'student.class_id')->select('student.*', 'classes.className')->where('student.addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.allstudents', $data);
    }

    public function add($id)
    {
        $data['parent']         =   $id;
        return view('schooladmin.modals.student.add_new_student' , $data);
    }
    
    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' =>  'required|email|unique:student|unique:users',
            'studentfName'  =>  'required',
            'studentlName'  =>  'required',
            'studentPass'  =>  'required|min:6',
            'studentPhone'  =>  'required',
            'studentAddress'  =>  'required',
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        
        $data = $request->all();
        $data['studentPass'] =  Hash::make($data['studentPass']);
        student::create($data);
        //user manage
        $user   =   new User();
        $user->name = $data['studentfName']." ".$data['studentlName'];
        $user->email = $data['email'];
        $data = $request->all();
        $user->password = Hash::make($data['studentPass']);
        $user->role     =   3;
        $user->save();

        Session::flash('message', 'student has been registered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        //delete student from user table
        $studentAuthID      =   Student::where('id', $id)->select('email')->get()->toArray();
        $deleteStudentUser  =   User::where('email', $studentAuthID[0]['email'])->delete();
        //delete student
        $data               =   Student::where('id', $id)->delete();
        Session::flash('message', 'student has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Student::where('id', $id)->get()->toArray();
        return view('schooladmin.modals.student.editstudent', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $student = Student::findOrFail($id);
        $upass = $input['studentPass'];
        if($input['studentPass'] != '')
            $input['studentPass'] = Hash::make($input['studentPass']);
        else
            unset($input['studentPass']);

            $validator = \Validator::make($request->all(), [
            'email' =>  'required|email|unique:student,email,'.$id,
            'studentfName'  =>  'required',
            'studentlName'  =>  'required',
            'studentPhone'  =>  'required',
            'studentAddress'  =>  'required', 
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $studentAuthID      =   Student::where('id', $id)->select('email')->get()->toArray();
        $student->fill($input)->save();
        //user manage
        $input = $request->all();
        $user = User::where('email', $studentAuthID[0]['email'])->first();
        $user->name = $input['studentfName']." ".$input['studentlName'];
        $user->email =  $input['email'];
        if( $upass != ''){
        $user->password = Hash::make($upass);
        }
        $user->save();
        Session::flash('message', $input['studentfName'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function detail($id)
    {
        $data['result']         =   Student::where('student.id', $id)->join('classes', 'classes.id', '=', 'student.class_id')->select('student.*', 'classes.className')->get()->toArray();
        return view('schooladmin.modals.student.studentdetail', $data);
    }
}