<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\School;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class SchoolController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Schools';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Schools');
        $data['result']         =   School::orderBy('id', 'DESC')->get()->toArray();
        return view('superadmin.allschools', $data);
    }

    public function add(Request $request)
    {
        return view('superadmin.modals.add_new_school');
    }
    
    public function save(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' =>  'required|email|unique:school|unique:users',
            'schoolName'  =>  'required',
            'adminName'   =>  'required',
            'schoolPass'  =>  'required|min:6',
            'totalClasses' => 'required',
            'totalStudents'=> 'required'
            
        ]);

        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        
        $data = $request->all();
        School::create($data);
        
        //user table manage
        $user   =   new User();
        $user->name = $data['adminName'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['schoolPass']);
        $user->role     =   1;
        $user->save();

        Session::flash('message', 'School has been registered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function detail($id)
    {
        $data['result']         =   School::where('id', $id)->get()->toArray();
        return view('superadmin.modals.schooldetail', $data);
    }

    public function delete($id)
    {
        //delete school from user table
        $schoolAuthID      =   School::where('id', $id)->select('email')->get()->toArray();
        $deleteSchoolUser  =   User::where('email', $schoolAuthID[0]['email'])->delete();
        //delete school
        $data              =   School::where('id', $id)->delete();
        Session::flash('message', 'School has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {
        $data['result']     =   School::where('id', $id)->get()->toArray();
        return view('superadmin.modals.editschool', $data);
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        $school = School::findOrFail($id);
        if($input['schoolPass'] != '')
            $input['schoolPass'] = Hash::make($input['schoolPass']);
        else
            unset($input['schoolPass']);

        $validator = \Validator::make($request->all(), [
            'schoolName'  =>  'required',
            'adminName'   =>  'required',
            'totalClasses' => 'required',
            'totalStudents'=> 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $school->fill($input)->save();
        //user table manage
        $user = User::where('email', $input['email'])->first();
        $user->name = $input['adminName'];
        $userpassword = $request->all();
        if($userpassword['schoolPass'] != '')
            $user->password = Hash::make($userpassword['schoolPass']);        
        $user->save();

        Session::flash('message', $input['schoolName'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }
}