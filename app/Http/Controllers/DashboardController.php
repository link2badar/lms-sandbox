<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\School;
use App\Instructor;
use App\Classes;
use App\Assign;
use App\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'Dashboard';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Dashboard');
        $data['result']         =   School::all()->toArray();
        $data['instructors']    =   Instructor::all()->toArray();
        $data['students']       =   Student::all()->toArray();
        


        //dashboard data for school
        if(Auth::user()->role == 1){
            $data['schoolInstructors']  =   Instructor::where('addedBy', Auth::user()->id)->get()->toArray();
            $data['schoolClasses']      =   Classes::where('addedBy', Auth::user()->id)->get()->toArray();
            $data['students']           =   Student::where('addedBy', Auth::user()->id)->get()->toArray();

        }
        if(Auth::user()->role == 2){
            $teacherAuthID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
            $data['courses']    =   Assign::where('teacherID', @$teacherAuthID[0]['id'])->get()->toArray();
        }
        if(Auth::user()->role == 3){
            $studentAuthID      =   Student::where('email', Auth::user()->email)->select('id', 'class_id')->get()->toArray();
            $data['courses']    =   Assign::whereRaw("find_in_set(".$studentAuthID[0]['id'].",studentID)")->where('is_group', 0)->get()->toArray();
            $data['coursesByGroup']    =   Assign::whereRaw("find_in_set(".$studentAuthID[0]['class_id'].",studentID)")->where('is_group', 1)->get()->toArray();
            
        }
        
        
        return view('superadmin.index', $data);
    }

    public function editProfile()
    {
        $data['pageName']       =   'View/Edit Profile';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Edit Profile');
        if(Auth::user()->role == 0){
            $data['userData']       =   User::where('email', Auth::user()->email)->get()->toArray();
            return view('superadmin.edit_profile', $data);
        }
        if(Auth::user()->role == 1){
            $data['userData']   =   School::where('email', Auth::user()->email)->get()->toArray();
            return view('schooladmin.edit_profile', $data);
        }
        if(Auth::user()->role == 2){
            $data['userData']   =   Instructor::where('email', Auth::user()->email)->get()->toArray();
            return view('instructor.edit_profile', $data);
        }
        if(Auth::user()->role == 3){
            $data['userData']   =   Student::where('email', Auth::user()->email)->get()->toArray();
            return view('student.edit_profile', $data);
        }
    }

    public function updateStudentProfile(Request $request)
    {  
        $input = $request->all();
        $student = Student::where('email', Auth::user()->email)->first();
        if($input['studentPass'] != '')
            $input['studentPass'] = Hash::make($input['studentPass']);
        else
            unset($input['studentPass']);
        //student update
        $student->fill($input)->save();
        //user manage
        $user = User::where('email', $input['email'])->first();
        $user->name = $input['studentfName'].' '.$input['studentmName'].' '.$input['studentlName'];
        $userpassword = $request->all();
        if($userpassword['studentPass'] != '')
            $user->password = Hash::make($userpassword['studentPass']);        
        $user->save();

        Session::flash('message', 'Profile has been updated successfully!'); 
        return redirect()->back();
    }
    
    public function updateInstructorProfile(Request $request)
    {  
        $input = $request->all();
        $instructor = Instructor::where('email', Auth::user()->email)->first();
        if($input['instructorPass'] != '')
            $input['instructorPass'] = Hash::make($input['instructorPass']);
        else
            unset($input['instructorPass']);
        $instructor->fill($input)->save();

        $user = User::where('email', $input['email'])->first();
        $user->name = $input['instructorfName'].' '.$input['instructormName'].' '.$input['instructorlName'];
        $userpassword = $request->all();
        if($userpassword['instructorPass'] != '')
            $user->password = Hash::make($userpassword['instructorPass']);        
        $user->save();

        Session::flash('message', 'Profile has been updated successfully!'); 
        return redirect()->back();
    }

    public function updateProfile(Request $request)
    {
        $input = $request->all();
        if(Auth::user()->role == 1) {
            $school = School::where('email', Auth::user()->email)->first();
            if($input['schoolPass'] != '')
                $input['schoolPass'] = Hash::make($input['schoolPass']);
            else
                unset($input['schoolPass']);
            $school->fill($input)->save();
        }
        $user = User::where('email', $input['email'])->first();
        $user->name = $input['adminName'];
        $userpassword = $request->all();
        if($userpassword['schoolPass'] != '')
            $user->password = Hash::make($userpassword['schoolPass']);        
        $user->save();


        Session::flash('message', 'Profile has been updated successfully!'); 
        return redirect()->back();
    }
}
