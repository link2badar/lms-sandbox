<?php
#Created By Badar
#date 18/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Lecture;
use App\Assign;
use App\Instructor;
use App\Lecturegrade;
use App\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use DB;
use ZipArchive;


class LectureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Lectures';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Lectures');
        $data['result']         =   Lecture::orderBy('lecture.id', 'DESC')->join('courses', 'courses.id', '=', 'lecture.courseID')->select('lecture.*', 'courses.courseName')->where('lecture.addedBy', Auth::user()->id)->get()->toArray();
        $data['gradeFilter']    =   Lecturegrade::where('addedBy', Auth::user()->id)->select('lectureID')->get()->toArray();
        return view('instructor.all_lectures', $data);
    }

    public function schoolindex()
    {
        $data['pageName']       =   'All Lectures';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Lectures');
        
        $data['result']         =   Lecture::orderBy('lecture.id', 'DESC')
        ->select('lecture.*', 'courses.courseName', 'users.name')
        ->join('courses', 'courses.id', '=', 'lecture.courseID')
        ->join('users', 'users.id', 'lecture.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->get()->toArray();
        
        return view('schooladmin.content.lectures', $data);
    }

    public function uploadLecture(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.modals.lecture.add_new_lecture', $data);
    }
    
    public function saveLecture(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Lecture::create($data);
        Session::flash('message', 'Lecture has been delivered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Lecture::where('id', $id)->delete();
        Session::flash('message', 'Lecture has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   Lecture::where('id', $id)->get()->toArray();
        return view('instructor.modals.lecture.edit_lecture', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        $lecture = Lecture::findOrFail($id);
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $lecture->fill($data)->save();
        Session::flash('message', 'Lecture: '.$data['title'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function download($archive_file_name)
    {
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile("$archive_file_name");
    }

    public function getStudentLectures($id)
    {
        $data['result']         =   Lecture::orderBy('lecture.id', 'DESC')->join('courses', 'courses.id', '=', 'lecture.courseID')->select('lecture.*', 'courses.courseName')->where('lecture.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'Lectures');
        return view('student.all_lectures', $data);
    }

    public function lectureGrade($id)
    {
        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('lecture.id' => $id, 'assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->join('lecture', 'lecture.courseID', '=', 'assign.courseID')->select('lecture.id', 'assign.studentID', 'lecture.title', 'lecture.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('lecture.id' => $id, 'assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->join('lecture', 'lecture.courseID', '=', 'assign.courseID')->select('lecture.id', 'assign.studentID', 'lecture.title', 'lecture.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"), DB::raw("GROUP_CONCAT(student.id) as sID"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();
       return view('instructor.modals.lecture.add_grade', $data);
    }
    
    public function saveLectureGrade(Request $request)
    {
        $data = $request->all();
        $data['studentID']          =   implode(",", $data['studentID']);
        $data['studentMarks']       =   implode(",", $data['studentMarks']);
        Lecturegrade::create($data);
        Session::flash('message', 'Grades has been submitted successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function allLectureGrade()
    {
        $data['pageName']       =   'All Lecture Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Lecture Grade');
        $data['result']         =   Lecturegrade::where('lecture_grade.addedBy', Auth::user()->id)->select('lecture_grade.*', 'lecture.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName") , DB::raw("GROUP_CONCAT(classes.className) as className") , DB::raw("GROUP_CONCAT(student.email) as studentEmail"))->join('lecture', 'lecture.id', '=', 'lecture_grade.lectureID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, lecture_grade.studentID)"),">",DB::raw("'0'"))->join('classes', 'classes.id', '=', 'student.class_id')->groupBy("lecture_grade.id")->get()->toArray();
        return view('instructor.grade.all_lecture', $data);
        
    }

    public function studentLectureGrade()
    {
        $data['pageName']       =   'All Lecture Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Lecture Grade');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Lecturegrade::whereRaw("find_in_set(".$studentID[0]['id'].",lecture_grade.studentID)")->select('lecture_grade.*', 'lecture.title')->join('lecture', 'lecture.id', '=', 'lecture_grade.lectureID')->get()->toArray();
        return view('student.grade.lecture', $data);
        
    }   
    
    public function schoolLectureGrade()
    {
        $data['pageName']       =   'All Lecture Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Lecture Grade');
        
        $data['result']         =   Lecturegrade::
        select('lecture_grade.*', 'lecture.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), 'users.name')
        ->join('lecture', 'lecture.id', '=', 'lecture_grade.lectureID')
        ->leftjoin("student",DB::raw("FIND_IN_SET(student.id, lecture_grade.studentID)"),">",DB::raw("'0'"))
        ->join('users', 'users.id', '=', 'lecture_grade.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->groupBy("lecture_grade.id")
        ->get()->toArray();

        return view('schooladmin.grade.lecture', $data);
        
    }   
}