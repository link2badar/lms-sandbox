<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Help;
use App\Helpdesk;
use App\Student; 
use App\Instructor;    
use App\Classes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class HelpController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

     //get all issues raised by student 
    public function studentHelp()
    {
        $data['pageName']       =   'Help Desk';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Desk');
        $data['result']         =   Help::orderBy('id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.addedBy', Auth::user()->id)->get()->toArray();
        return view('student.help.allhelps', $data);
    }

    public function schoolHelp()
    {
        $data['pageName']       =   'Help Desk';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Desk');
        $data['result']         =   Help::orderBy('id', 'DESC')
        ->join('users', 'users.id', '=', 'help.addedBy')
        ->join('student', 'student.email', '=', 'users.email')
        ->join('classes', 'classes.id', '=', 'student.class_id')
        ->select('help.*', 'users.name', 'users.email', 'classes.className')
        ->where('help.submitTo', Auth::user()->id)
        ->get()->toArray();
        return view('schooladmin.help.allhelps', $data);
    }
    
    public function registerStudentHelp()
    {
        return view('student.help.add_new_help');
    }

    public function submitStudentHelp(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'subject'       =>  'required',
            'description'   =>  'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $submitTo       =   Student::where('email', Auth::user()->email)->select('addedBy')->get()->toArray();
        $data           =   $request->all();
        $data['submitTo']   =   $submitTo[0]['addedBy'];


        Help::create($data);
        Session::flash('message', 'Issue has been Submitted To school Admin successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function helpDetail($id)
    {
        $data['pageName']       =   'Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('student.help.help_detail', $data);
    }

    public function schoolHelpDetail($id)
    {
        $data['pageName']       =   'Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.addedBy')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('schooladmin.help.help_detail', $data);
    }

    public function schoolHelpIntervalChat($id)
    {
        //$data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.addedBy')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return Response()->json($data);
    }
    
    public function helpIntervalChat($id)
    {
        //$data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return Response()->json($data);
    }

    public function saveHelpDetail(Request $request)
    {
        $data           =   $request->all();
        Helpdesk::create($data);
        $returnData['senderName']       =   $data['senderName'];
        $returnData['senderDate']       =   date("d/M/y h:i");
        $returnData['description']      =   $data['description'];
        return Response()->json($returnData);
        
    }

     

     //get all issues raised by instructor 
    public function instructorHelp()
    {
        $data['pageName']       =   'Help Desk';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Desk');
        $data['result']         =   Help::orderBy('id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.addedBy', Auth::user()->id)->get()->toArray();
        return view('instructor.help.allhelps', $data);
    }


     public function registerInstructorHelp()
    {
        return view('instructor.help.add_new_help');
    }

    public function submitInstructorHelp(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'subject'       =>  'required',
            'description'   =>  'required',
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }

        $submitTo       =   Instructor::where('email', Auth::user()->email)->select('addedBy')->get()->toArray();
        $data           =   $request->all();
        $data['submitTo']   =   $submitTo[0]['addedBy'];


        Help::create($data);
        Session::flash('message', 'Issue has been Submitted To school Admin successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function ihelpDetail($id)
    {
        $data['pageName']       =   'Instructor Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.submitTo')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('instructor.help.help_detail', $data);
    }


    public function saveiHelpDetail(Request $request)
    {
        $data           =   $request->all();
        Helpdesk::create($data);
        $returnData['senderName']       =   $data['senderName'];
        $returnData['senderDate']       =   date("d/M/y h:i");
        $returnData['description']      =   $data['description'];
        return Response()->json($returnData);
        
    }

    public function schooliHelp()
    {
        $data['pageName']       =   'Instructor Help Desk';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Desk');
        $data['result']         =   Help::orderBy('id', 'DESC')
        ->join('users', 'users.id', '=', 'help.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->select('help.*', 'users.name', 'users.email')
        ->where('help.submitTo', Auth::user()->id)
        ->get()->toArray();
        return view('schooladmin.help.allihelps', $data);
    }

    public function schooliHelpDetail($id)
    {
       
        $data['pageName']       =   'Instructor Help Discussion';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'Help Instructor Discussion');

        $data['helpData']       =   Help::orderBy('help.id', 'DESC')->join('users', 'users.id', '=', 'help.addedBy')->select('help.*', 'users.name')->where('help.id', $id)->get()->toArray();

        $data['result']         =   Helpdesk::where('helpID', $id)->get()->toArray();

        return view('schooladmin.help.ihelp_detail', $data);
    }
}