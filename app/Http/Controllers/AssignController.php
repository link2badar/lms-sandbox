<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Classes;
use App\Course;
use App\Assign;
use App\Student;
use App\Instructor;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;


class AssignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Assigns';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assign');
        $data['result']         =   Assign::orderBy('assign.id', 'DESC')->join('instructor', 'instructor.id', '=', 'assign.teacherID')->join('courses', 'courses.id', '=', 'assign.courseID')->select('assign.*', 'instructor.instructorfName', 'instructor.instructormName', 'instructor.instructorlName', 'courses.courseName')->where('assign.addedBy', Auth::user()->id)->get()->toArray();
        $data['students']       =   Student::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['classes']        =   Classes::where('addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.allassign', $data);
    }

    public function add(Request $request)
    {
        $data['instructor']     =   Instructor::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['courses']        =   Course::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['students']       =   Student::where('student.addedBy', Auth::user()->id)->join('classes', 'classes.id', '=', 'student.class_id')->select('student.*', 'classes.className')->get()->toArray();
        $data['classes']        =   Classes::where('addedBy', Auth::user()->id)->get()->toArray();
        
        return view('schooladmin.modals.assign.assign_new_instructor', $data);
    }
    
    public function saveAssign(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'teacherID'  =>  'required',
            'courseID'  =>  'required',
            'studentID'  =>  'required_without:groupID',
            'groupID'   =>  'required_without:studentID'
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        
        
        if($data['group'] == 'group'){
            $data['is_group']   =   1;
            $data['studentID']  =   $data['groupID'];
        }
        $data['studentID'] = implode(',', $data['studentID'] );
        unset($data['groupID']);
        unset($data['group']);
        Assign::create($data);
        Session::flash('message', 'Instructor Assigned successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   Assign::where('id', $id)->delete();
        Session::flash('message', 'Assign instructor has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {
        $data['result']         =   Assign::where('id', $id)->get()->toArray();
        $data['instructor']     =   Instructor::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['courses']        =   Course::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['students']       =   Student::where('addedBy', Auth::user()->id)->get()->toArray();
        $data['classes']        =   Classes::where('addedBy', Auth::user()->id)->get()->toArray();
        return view('schooladmin.modals.assign.edit_assign', $data);
    }

    public function updateAssign(Request $request, $id)
    {
        $input = $request->all();
        $classes = Assign::findOrFail($id);
        $validator = \Validator::make($request->all(), [
            'teacherID'  =>  'required',
            'courseID'  =>  'required',
            'studentID'  =>  'required_without:groupID',
            'groupID'   =>  'required_without:studentID'
        ]);
        if ($validator->fails())
        {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        if($input['group'] == 'group'){
            $input['is_group']   =   1;
            $input['studentID']  =   $input['groupID'];
        }
        $input['studentID'] = implode(',', $input['studentID'] );
        unset($input['groupID']);
        unset($input['group']);
        
        $classes->fill($input)->save();

        Session::flash('message', 'Assigned Instructor has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }


    //get Assigned courses to instructor
    public function getAssignedCourses()
    {
        $data['pageName']       =   'All Assigned Courses';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assigned Courses');
        $teacherAuthID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['result']     =   Assign::where('teacherID', $teacherAuthID[0]['id'])->join('courses', 'courses.id', '=', 'assign.courseID')->orderBy('assign.id', 'Desc')->get()->toArray();
        return view('instructor.allcourses', $data);
    }

    //get enrolled courses for the students
    public function getEnrolledCourses()
    {
        $data['pageName']       =   'All Enrolled Courses';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Enrolled Courses');

        
        $studentAuthClassID     =   Student::where('email', Auth::user()->email)->select('class_id')->get()->toArray();
        $data['assignedCoursesByGroup']=   Assign::select('assign.*', 'courses.courseName')->whereRaw("find_in_set(".$studentAuthClassID[0]['class_id'].",assign.studentID)")->join('courses', 'courses.id', '=', 'assign.courseID')->where('assign.is_group', '1')->orderBy('assign.id', 'DESC')->get()->toArray();

        $studentAuthID          =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['assignedCourses']=   Assign::select('assign.*', 'courses.courseName')->whereRaw("find_in_set(".$studentAuthID[0]['id'].",assign.studentID)")->join('courses', 'courses.id', '=', 'assign.courseID')->where('assign.is_group', '0')->orderBy('assign.id', 'DESC')->get()->toArray();
        
        return view('student.allenrolledcourses', $data);
    }
}