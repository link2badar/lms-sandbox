<?php
#Created By Sohail
#date 19/8/2020
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Course;
use App\Assignment;
use App\Submitassignment;
use App\Assignmentgrade;
use App\Instructor;
use App\Assign;
use App\Student;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;
use Response;
use ZipArchive;
use DB;


class AssignmentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['pageName']       =   'All Assignment';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment');
        $data['result']         =   assignment::orderBy('assignment.id', 'DESC')->join('courses', 'courses.id', '=', 'assignment.courseID')->select('assignment.*', 'courses.courseName')->where('assignment.addedBy', Auth::user()->id)->get()->toArray();
        $data['gradeFilter']    =   Assignmentgrade::where('addedBy', Auth::user()->id)->select('assignmentID')->get()->toArray();
        return view('instructor.assignment.all_assignments', $data);
    }

    public function schoolindex()
    {
        $data['pageName']       =   'All Assignments';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignments');
        
        $data['result']         =   Assignment::orderBy('assignment.id', 'DESC')
        ->select('assignment.*', 'courses.courseName', 'users.name')
        ->join('courses', 'courses.id', '=', 'assignment.courseID')
        ->join('users', 'users.id', 'assignment.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->get()->toArray();
        
        return view('schooladmin.content.assignments', $data);
    }

    public function uploadassignment(Request $request, $id)
    {
        $data['result']             =       Course::where('id', $id)->get()->toArray();
        return view('instructor.assignment.add_new_assignment', $data);
    }
    
    public function saveassignment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description',
            'deadline' => 'required',
            'deadlineTime' => 'required'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $data['deadline']       =   date("Y-m-d H:i", strtotime($data['deadline'].' '.$data['deadlineTime']));
        unset($data['deadlineTime']);
        assignment::create($data);
        Session::flash('message', 'assignment has been delivered successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function delete($id)
    {
        $data                   =   assignment::where('id', $id)->delete();
        Session::flash('message', 'assignment has been deleted successfully!');
        Session::flash('danger', true);
        return redirect()->back();
    }

    public function edit($id)
    {

        $data['result']     =   assignment::where('id', $id)->get()->toArray();
        return view('instructor.assignment.edit_assignment', $data);
    }

    public function update(Request $request, $id)
    {
        $validator = \Validator::make($request->all(), [
            'title'  =>  'required',            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        $assignment = assignment::findOrFail($id);
        if ($files = $request->file('file')) {

            $zipname = $data['title'].'-'.rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        $assignment->fill($data)->save();
        Session::flash('message', 'assignment: '.$data['title'].' has been updated successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function download($archive_file_name)
    {
        header("Content-type: application/zip"); 
        header("Content-Disposition: attachment; filename=$archive_file_name");
        header("Content-length: " . filesize($archive_file_name));
        header("Pragma: no-cache"); 
        header("Expires: 0"); 
        readfile("$archive_file_name");
    }

    public function getStudentAssignments($id)
    {
        $data['result']         =   Assignment::orderBy('assignment.id', 'DESC')->join('courses', 'courses.id', '=', 'assignment.courseID')->select('assignment.*', 'courses.courseName')->where('assignment.courseID', $id)->get()->toArray();
        $data['course']         =   Course::where('id', $id)->select('courseName')->get()->toArray();
        $data['pageName']       =   'Course Name: '.$data['course'][0]['courseName'];
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-enrolled-courses" => 'All Enrolled Courses', '#' => 'All Assignments');
        $data['submissionStatus']=  Submitassignment::where('submittedBy', Auth::user()->id)->get()->toArray();
        return view('student.all_assignments', $data);
    }

    public function assignmentSubmit($id)
    {
        $data['id']         =       $id;
        $data['result']     =       Assignment::where('id', $id)->get()->toArray();
        return view('student.modal.assignment_submit', $data);
    }

    public function submitAssignmentByStudent(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'description'  =>  'required_without:file',
            'file'   =>  'required_without:description'
            
        ]);
        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()->all()]);
        }
        $data = $request->all();
        if ($files = $request->file('file')) {

            $zipname = rand().date('YmdHis').'.zip';
            $zip = new ZipArchive();
            $zip->open($zipname,  ZipArchive::CREATE);
            foreach($files as $file){

            $destinationPath = public_path('/uploadedFile/'); // upload path
            $profileImage = rand().date('YmdHis') . "." . $file->getClientOriginalExtension();
            $file->move($destinationPath, $profileImage);
            $path = public_path('uploadedFile/').$profileImage;
            if(file_exists($path)){
            $zip->addFromString(basename($path),  file_get_contents($path)); 
            unlink($path); 
            }
        }
            $data['file'] = $zipname;
        }
        Submitassignment::create($data);
        Session::flash('message', 'assignment has been uploaded successfully!'); 
        return Response()->json(['success' => '1']);
        
    }

    public function submissionList($id)
    {
        $data['result']         =   Submitassignment::orderBy('submit_assignment.id', 'DESC')->join('assignment', 'assignment.id', '=', 'submit_assignment.assignmentID')->join('users', 'users.id', '=', 'submit_assignment.submittedBy')->select('submit_assignment.*', 'assignment.title', 'users.name', 'assignment.deadline')->where('submit_assignment.assignmentID', $id)->get()->toArray();
        $data['pageName']       =   'Assignment';
        $data['breadcrumbs']    =   array("dashboard" => "Home", "all-assignments" => 'All Assignments', '#' => '');
        if(count($data['result'])>0){
            $data['pageName']       =   'Assignment: '.$data['result'][0]['title'];
            $data['breadcrumbs']    =   array("dashboard" => "Home", "all-assignments" => 'All Assignments', '#' => $data['result'][0]['title']);
        }
        return view('instructor.assignment.all_submitted_assignments', $data);
    }

    public function assignmentGrade($id)
    {
        $teacherID      =   Instructor::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['studentList']    =   Assign::where(array('assignment.id' => $id, 'assign.is_group' => '0', 'assign.teacherID' => $teacherID[0]['id']))->join('assignment', 'assignment.courseID', '=', 'assign.courseID')->select('assignment.id', 'assign.studentID', 'assignment.title', 'assignment.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"))->leftjoin("student",DB::raw("FIND_IN_SET(student.id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        //student list by group
        $data['studentListByGroup']    =   Assign::where(array('assignment.id' => $id, 'assign.is_group' => '1', 'assign.teacherID' => $teacherID[0]['id']))->join('assignment', 'assignment.courseID', '=', 'assign.courseID')->select('assignment.id', 'assign.studentID', 'assignment.title', 'assignment.created_at', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(student.email) as email"), DB::raw("GROUP_CONCAT(student.id) as sID"))->leftjoin("student",DB::raw("FIND_IN_SET(student.class_id,assign.studentID)"),">",DB::raw("'0'"))
        ->groupBy("assign.id")->get()->toArray();

        return view('instructor.assignment.add_grade', $data);
    }
    
    public function saveAssignmentGrade(Request $request)
    {
        $data = $request->all();
        $data['studentID']          =   implode(",", $data['studentID']);
        $data['studentMarks']       =   implode(",", $data['studentMarks']);
        Assignmentgrade::create($data);
        Session::flash('message', 'Grades has been submitted successfully!'); 
        return Response()->json(['success' => '1']);
    }

    public function allAssignmentGrade()
    {
        $data['pageName']       =   'All Assignment Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment Grade');
        $data['result']         =   Assignmentgrade::where('assignment_grade.addedBy', Auth::user()->id)->select('assignment_grade.*', 'assignment.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), DB::raw("GROUP_CONCAT(classes.className) as className") , DB::raw("GROUP_CONCAT(student.email) as studentEmail"))->join('assignment', 'assignment.id', '=', 'assignment_grade.assignmentID')->leftjoin("student",DB::raw("FIND_IN_SET(student.id, assignment_grade.studentID)"),">",DB::raw("'0'"))->join('classes', 'classes.id', '=', 'student.class_id')->groupBy("assignment_grade.id")->get()->toArray();
        return view('instructor.assignment.all_assignments_grade', $data);
        
    }

    public function studentAssignmentGrade()
    {
        $data['pageName']       =   'All Assignment Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment Grade');
        $studentID              =   Student::where('email', Auth::user()->email)->select('id')->get()->toArray();
        $data['sID']            =   @$studentID[0]['id'];
        $data['result']         =   Assignmentgrade::whereRaw("find_in_set(".$studentID[0]['id'].",assignment_grade.studentID)")->select('assignment_grade.*', 'assignment.title')->join('assignment', 'assignment.id', '=', 'assignment_grade.assignmentID')->get()->toArray();
        return view('student.grade.assignment', $data);
        
    }

    public function schoolAssignmentGrade()
    {
        
        $data['pageName']       =   'All Assignment Grade';
        $data['breadcrumbs']    =   array("dashboard" => "Home", '#' => 'All Assignment Grade');
        
        $data['result']         =   Assignmentgrade::
        select('assignment_grade.*', 'assignment.title', DB::raw("GROUP_CONCAT(student.studentfName) as studentName"), 'users.name')
        ->join('assignment', 'assignment.id', '=', 'assignment_grade.assignmentID')
        ->leftjoin("student",DB::raw("FIND_IN_SET(student.id, assignment_grade.studentID)"),">",DB::raw("'0'"))
        ->join('users', 'users.id', '=', 'assignment_grade.addedBy')
        ->join('instructor', 'instructor.email', '=', 'users.email')
        ->where('instructor.addedBy', Auth::user()->id)
        ->groupBy("assignment_grade.id")
        ->get()->toArray();

        return view('schooladmin.grade.assignment', $data);
        
    }
}