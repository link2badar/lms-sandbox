<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Quizgrade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'quiz_grade';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'quizID', 'studentID', 'studentMarks', 'quizDate', 'addedBy'
    ];
}