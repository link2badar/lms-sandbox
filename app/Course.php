<?php
#Created By sohail
#date 12/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Course extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'courses';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'courseName', 'addedBy'
    ];
}