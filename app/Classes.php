<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Classes extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'classes';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'className', 'addedBy', 'parent_id'
    ];
}