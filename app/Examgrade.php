<?php
#Created By Badar
#date 18/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Examgrade extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'exam_grade';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'examID', 'studentID', 'studentMarks', 'examDate', 'addedBy'
    ];
}