<?php
#Created By sohail
#date 12/8/2020

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Chat extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'chat';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'id',	'sendBy',	'sendTo',	'senderName',	'receiverName',	'description'
    ];
}