<?php
#Created By sohail
#date 12/8/2020
namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Student extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'student';
    
    protected $primarykey='id';
    
    protected $fillable = [
        'studentfName', 'studentmName','studentlName', 'email', 'studentPass', 'studentPhone', 'studentAddress','addedBy','class_id'
    ];
}