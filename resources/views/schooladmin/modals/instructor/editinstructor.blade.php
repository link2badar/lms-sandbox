<!-- Created By sohail
date 12/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Update {{ @$result[0]['instructorfName']." ".@$result[0]['instructorlName'] }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-instructor/'.@$result[0]['id']) }}" id="InstructorRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="instructorfName">First Name</label>
                <input type="text" class="form-control" id="instructorfName" placeholder="Enter First Name" required
                    name="instructorfName" value="{{ @$result[0]['instructorfName']}}"/>
            </div>
            <div class="form-group">
                <label for="instructormName">Middle Name</label>
                <input type="text" class="form-control" id="instructormName" placeholder="Enter Middle Name" required
                    name="instructormName" value="{{ @$result[0]['instructormName']}}"/>
            </div>
            <div class="form-group">
                <label for="instructorlName">Last Name</label>
                <input type="text" class="form-control" id="instructorlName" placeholder="Enter Last Name" required
                    name="instructorlName" value="{{ @$result[0]['instructorlName']}}"/>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Instructor Email" required
                    name="email" readonly value="{{ @$result[0]['email']}}">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter Instructor Password"
                    required name="instructorPass" >
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="number" class="form-control" id="phone" placeholder="Enter Phone #"
                    name="instructorPhone" value="{{ @$result[0]['instructorPhone']}}"/>
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="instructorAddress" >{{ @$result[0]['instructorAddress']}}</textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var id = "<?php echo $result[0]['id']; ?>";
    $.ajax({
        type: "POST",
        url: site_url + "/update-instructor/"+id,
        data:  $("#InstructorRegistration").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');
                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}
</script>