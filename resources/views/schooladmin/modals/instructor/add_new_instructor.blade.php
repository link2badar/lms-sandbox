<!-- Created By sohail
date 12/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Register New Instructor</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/save-new-instructor') }}" id="InstructorRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="instructorfName">First Name</label>
                <input type="text" class="form-control" id="instructorfName" placeholder="Enter First Name" required
                    name="instructorfName" />
            </div>
           <div class="form-group">
                <label for="instructormName">Middle Name</label>
                <input type="text" class="form-control" id="instructormName" placeholder="Enter Middle Name" required
                    name="instructormName" />
            </div>
            <div class="form-group">
                <label for="instructorlName">Last Name</label>
                <input type="text" class="form-control" id="instructorlName" placeholder="Enter Last Name" required
                    name="instructorlName" />
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Instructor Email" required
                    name="email">
            </div>
            <div class="form-group">
                <?php $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';?>
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter Instructor Password"
                    required name="instructorPass" value="{{ substr(str_shuffle($permitted_chars), 0, 7)}}">
                    <span class='help-desk'>Auto Generated Password: {{ substr(str_shuffle($permitted_chars), 0, 7)}}</span>
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="number" class="form-control" value="" id="phone" placeholder="Enter Phone #"
                    name="instructorPhone" />
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="instructorAddress"></textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    $.ajax({
        type: "POST",
        url: site_url + "/save-new-instructor",
        data:  $("#InstructorRegistration").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}
</script>