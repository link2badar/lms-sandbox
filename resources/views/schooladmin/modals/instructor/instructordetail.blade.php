<div class="modal-header">
    <h4 class="modal-title">Instructor: {{ @$result[0]['instructorfName']." ".@$result[0]['instructorlName'] }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-instructor/'.@$result[0]['id']) }}" id="InstructorRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="instructorfName">First Name</label>
                <input type="text" class="form-control" id="instructorfName" placeholder="Enter First Name" required
                    name="instructorfName" disabled="disabled" value="{{ @$result[0]['instructorfName']}}"/>
            </div>
            <div class="form-group">
                <label for="instructorlName">Middle Name</label>
                <input type="text" class="form-control" id="instructormName" placeholder="Enter Middle Name" required
                    name="instructormName" disabled="disabled" value="{{ @$result[0]['instructormName']}}"/>
            </div>
            <div class="form-group">
                <label for="instructorlName">Last Name</label>
                <input type="text" class="form-control" id="instructorlName" placeholder="Enter Last Name" required
                    name="instructorlName" disabled="disabled" value="{{ @$result[0]['instructorlName']}}"/>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter Instructor Email" required
                    name="email" disabled="disabled" value="{{ @$result[0]['email']}}">
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="text" class="form-control" id="phone" placeholder="Enter Phone #"
                    name="instructorPhone" disabled="disabled" value="{{ @$result[0]['instructorPhone']}}"/>
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="instructorAddress" disabled="disabled" >{{ @$result[0]['instructorAddress']}}</textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>