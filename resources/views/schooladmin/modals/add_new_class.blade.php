<div class="modal-header">
    <h4 class="modal-title">Register New Group</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<form role="form" id="classRegistration" method="post">
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="className">Group Name</label>
                <input type="text" class="form-control" id="className" placeholder="Enter Group Name" required
                    name="className" />
            </div>
        </div>
        <!-- /.card-body -->
    
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>
</form>
<script>
 $("#classRegistration").submit(function(){
    $.ajax({
        type: "POST",
        url: site_url + "/save-new-class",
        data:  $("#classRegistration").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
});
</script>