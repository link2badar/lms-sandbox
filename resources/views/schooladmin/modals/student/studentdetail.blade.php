<div class="modal-header">
    <h4 class="modal-title">Student: {{ @$result[0]['studentfName']." ".@$result[0]['studentmName'] }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-student/'.@$result[0]['id']) }}" id="studentRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="studentfName">First Name</label>
                <input type="text" class="form-control" id="studentfName" placeholder="Enter First Name" required
                    name="studentfName" disabled="disabled" value="{{ @$result[0]['studentfName']}}"/>
            </div>
            <div class="form-group">
                <label for="studentlName">Middle Name</label>
                <input type="text" class="form-control" id="studentmName" placeholder="Enter Middle Name" required
                    name="studentmName" disabled="disabled" value="{{ @$result[0]['studentmName']}}"/>
            </div>
            <div class="form-group">
                <label for="studentlName">Last Name</label>
                <input type="text" class="form-control" id="studentlName" placeholder="Enter Last Name" required
                    name="studentlName" disabled="disabled" value="{{ @$result[0]['studentlName']}}"/>
            </div>
            <div class="form-group">
                <label for="studentlName">Group</label>
                <input type="text" class="form-control" id="className" required
                    name="className" disabled="disabled" value="{{ @$result[0]['className']}}"/>
            </div>
            
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter student Email" required
                    name="studentEmail" disabled="disabled" value="{{ @$result[0]['email']}}">
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="text" class="form-control" id="phone" placeholder="Enter Phone #"
                    name="studentPhone" disabled="disabled" value="{{ @$result[0]['studentPhone']}}"/>
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="studentAddress" disabled="disabled" >{{ @$result[0]['studentAddress']}}</textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>