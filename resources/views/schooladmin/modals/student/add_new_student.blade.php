<!-- Created By sohail
date 12/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Register New Student</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/save-new-student') }}" id="studentRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="studentfName">First Name</label>
                <input type="text" class="form-control" id="studentfName" placeholder="Enter First Name" required
                    name="studentfName" />
                    <input type="hidden" name="class_id" value="{{ @$parent }}" />
            </div>
             <div class="form-group">
                <label for="studentmName">Middle Name</label>
                <input type="text" class="form-control" id="studentmName" placeholder="Enter Middle Name" required
                    name="studentmName" />
            </div>
            <div class="form-group">
                <label for="studentlName">Last Name</label>
                <input type="text" class="form-control" id="studentlName" placeholder="Enter Last Name" required
                    name="studentlName" />
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter student Email" required
                    name="email">
            </div>
            <div class="form-group">
            <?php $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyz';?>
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter student Password"
                    required name="studentPass" value="{{ substr(str_shuffle($permitted_chars), 0, 7)}}">
                    <span class='help-desk'>Auto Generated Password: {{ substr(str_shuffle($permitted_chars), 0, 7)}}</span>
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="number" class="form-control" value="" id="phone" placeholder="Enter Phone #"
                    name="studentPhone" />
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="studentAddress"></textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    $.ajax({
        type: "POST",
        url: site_url + "/save-new-student",
        data:  $("#studentRegistration").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}
</script>