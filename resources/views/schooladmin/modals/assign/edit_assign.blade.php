<!-- Created By sohail
date 12/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Update Data</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-assign/'.@$result[0]['id']) }}" id="assignInstructorUpdate">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
        <div class="form-group">
                <label>Instructor</label>
                <select class="form-control select2" name="teacherID" style="width: 100%;">
                    @if(!empty($instructor)) 
                        @foreach($instructor as $key => $value)
                        <?php $select = '';
                         if($value['id'] == $result[0]['teacherID']) $select = 'selected="selected"'; ?>
                            <option {{$select}} value="{{$value['id']}}">{{$value['instructorfName'].' '.$value['instructormName'].' '.$value['instructorlName']}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
                <label>Course</label>
                <select class="form-control select2" name="courseID" style="width: 100%;">
                    @if(!empty($courses)) <?php $counter = 1;?>
                        @foreach($courses as $key => $value)
                        <?php $select = '';
                         if($value['id'] == $result[0]['courseID']) $select = 'selected="selected"'; ?>
                            <option {{$select}} value="{{$value['id']}}">{{$value['courseName']}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group">
            <label>Selection of Students By</label>
            <div class="form-check">
                        <?php $groupCheck = ''; $studentCheck = ''; if($result[0]['is_group']){ $groupCheck ='checked="checked"';} else {$studentCheck = 'checked="checked"';}?>
                          <input class="form-check-input" type="radio" name="group" value="student" {{$studentCheck}}>
                          <label class="form-check-label">Student</label>
                        </div>
                        <div class="form-check">
                          <input class="form-check-input" type="radio" name="group" value="group" {{$groupCheck}}>
                          <label class="form-check-label">Group</label>
                        </div>
                        
                      </div>
                       

            <div class="form-group" id="studentFilter"  @if($result[0]['is_group'] == 1) <?php echo 'style="display:none;"';?> @endif>
                <label>Students</label>
                <select class="select2" multiple="multiple" data-placeholder="Select Multiple Students" name="studentID[]" style="width: 100%;">
                    @if(!empty($students)) 
                        <?php $counter = 1; $studentID = explode(',',$result[0]['studentID']);?>
                        @foreach($students as $key => $value)
                        <?php $select = '';
                         if(in_array($value['id'], $studentID)) $select = 'selected="selected"'; ?>
                            <option {{$select}} value="{{$value['id']}}">{{$value['email'].'  (  '.$value['studentfName'].'  )'}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="form-group" id="groupFilter"  @if($result[0]['is_group'] == 0) <?php echo 'style="display:none;"';?> @endif>
                <label>Groups</label>
                <select class="select2" multiple="multiple" data-placeholder="Select Multiple Groups" name="groupID[]" style="width: 100%;">
                    @if(!empty($classes)) 
                        <?php $counter = 1; $studentID = explode(',',$result[0]['studentID']);?>
                        @foreach($classes as $key => $value)
                        <?php $select = '';
                         if(in_array($value['id'], $studentID)) $select = 'selected="selected"'; ?>
                            <option {{$select}} value="{{$value['id']}}">{{$value['className']}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var id = "<?php echo $result[0]['id']; ?>";
    $.ajax({
        type: "POST",
        url: site_url + "/update-assign/"+id,
        data:  $("#assignInstructorUpdate").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');
                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}

$(function () {
    //Initialize Select2 Elements
    $('.select2').select2();
});

$('input[type=radio][name=group]').change(function() {
    if (this.value == 'student') {
        $("#groupFilter").hide();
        $("#studentFilter").show();
    }
    else if (this.value == 'group') {
        $("#groupFilter").show();
        $("#studentFilter").hide();
    }
});
</script>