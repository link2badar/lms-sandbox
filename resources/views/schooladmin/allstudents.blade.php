@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Students</h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                        <!-- <button type="button" class="btn btn-primary" onclick="loadModal( '/register-new-student')" data-toggle="modal" data-target="#modal-lg">
                            Register New Student
                        </button> -->
                    </li>
                  </ul>
                </div>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>First Name</th>
                    <th>Middle Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Group</th>
                    <th>Detail</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$value['studentfName']}}</td>
                     <td>{{$value['studentmName']}}</td>
                     <td>{{$value['studentlName']}}</td>
                    <td>{{$value['email']}}</td>
                    <td>{{$value['className']}}</td>
                    <td><button class="btn btn-block btn-outline-success btn-sm" onclick="loadModal('/student/detail/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">{{'more detail'}}</button></td>
                    
                    <td>
                    <button type="button" class="btn bg-gradient-primary btn-sm" onclick="loadModal('/edit-student/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Edit</button>
 |
                        <a href="{{ url('/delete-student/'.$value['id']) }}"><button type="button" class="btn bg-gradient-danger btn-sm">Delete</button>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
