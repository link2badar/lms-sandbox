@extends('layouts.adminapp')

@section('content')
<script>
function formSubmit() {
    if ($(".msgDetail").val() == "") {
        alert("You can't send empty message");
        return false;
    }
    $.ajax({
        type: "POST",
        url: site_url + "/save-help-detail",
        data: $(".sendHelpMsg").serialize(),
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function (result) {
        
            var appendMsg = '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+result.senderName+'</span><span class="direct-chat-timestamp float-left">'+result.senderDate+'</span></div><div class="direct-chat-text">'+result.description+'</div></div>';
            $(".senderSide").append(appendMsg);
            $(".msgDetail").val('');
            $(".direct-chat-messages").animate({
    scrollTop: $(".direct-chat-messages").get(0).scrollHeight,
});
            }
    });
}


setInterval(ajaxCall, 3000); //300000 MS == 5 minutes
var authID = '{{Auth::user()->id}}';
function ajaxCall() {
  $.ajax({
    url: site_url + "/school-help-intervalChat/{{@$helpData[0]['id']}}",
    dataType: "json",
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
    success: function (result) {
      var reloadContent = '';
      $.each(result, function(key, ovalue) {
          $.each(ovalue, function(ikey, value) {  
          if(value['sendBy'] != authID){
            reloadContent += '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+value['senderName']+'</span><span class="direct-chat-timestamp float-right">'+value['created_at']+'</span></div><div class="direct-chat-text">'+value['description']+'</div></div>';
          }
          else{
            reloadContent   +=  '<div class="senderSide"><div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+value['senderName']+'</span><span class="direct-chat-timestamp float-left">'+value['created_at']+'</span></div><div class="direct-chat-text">'+value['description']+'</div></div></div>';
          }
        });
      });
      $("#msgList").html(reloadContent);
    }
  });
}


// var counter = 1;
// var auto_refresh = setInterval(
// function () {
//     var newcontent= $("#msgList").html();
//     $('.card-body').html(newcontent);
//     counter++;
// }, 4000);

// setInterval(function(){ console.log("tesT"); $(".card-body").load("#msgList", 3000) });

</script>
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">

<div class="card card-info direct-chat direct-chat-info" style="height: 425px;">
  <div class="card-header">
    <h3 class="card-title">Subject: {{@$helpData[0]['subject']}}</h3><br />
    <p>Detail: {{@$helpData[0]['description']}}</p>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages" id="msgList">

        @if(!empty($result))
        @foreach($result as $key => $value)

        @if($value['sendBy'] != Auth::user()->id)
      <!-- Message. Default to the left -->
      <div class="direct-chat-msg">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-left">{{@$value['senderName']}}</span>
          <span class="direct-chat-timestamp float-right">{{@date("d/M/y h:i A", strtotime($value['created_at'])) }} </span>
        </div>
        <div class="direct-chat-text">
        {{$value['description']}}
        </div>
      </div>
      @else
      <!-- Message to the right -->
      <div class="senderSide">
      <div class="direct-chat-msg right">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-right">{{@$value['senderName']}}</span>
          <span class="direct-chat-timestamp float-left">{{@date("d/M/y h:i A", strtotime($value['created_at']))}}</span>
        </div>
        <div class="direct-chat-text">
          {{@$value['description']}}
        </div>
      </div>
      </div>
      @endif

      @endforeach
      @endif
      <!-- /.direct-chat-msg -->
    </div>
    <!--/.direct-chat-messages-->

  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <form action="#" method="post" class="sendHelpMsg">
    <input type="hidden" name="sendTo" value="{{@$helpData[0]['addedBy']}}" />
    <input type="hidden" name="sendBy" value="{{ Auth::user()->id}}" />
    <input type="hidden" name="senderName" value="{{ Auth::user()->name}}" />
    <input type="hidden" name="receiverName" value="{{@$helpData[0]['name']}}" />
    <input type="hidden" name="helpID" value="{{@$helpData[0]['id']}}" />
    
      <div class="input-group">
        <textarea name="description" placeholder="Type Message ..." class="form-control msgDetail"></textarea>
        <span class="input-group-append">
          <button type="button" class="btn btn-primary" onclick="formSubmit()">Send</button>
        </span>
      </div>
    </form>
  </div>
  <!-- /.card-footer-->
</div>
<!--/.direct-chat -->
</div>
</div>
</div>
</div>

<script>

                </script>
@endsection
