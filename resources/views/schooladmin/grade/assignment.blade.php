@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Assignment  Grading System</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-striped">
                 



<thead>
  <tr>
    <th>Assignment Title</th>
    <th>Assignment Date</th>
    <th>Instructor Name</th>
    <th>Student Names</th>
    <th>Student Marks</th>
  </tr>
</thead>

<tbody>
@if(!empty($result))
  @foreach($result as $key => $value)
    <?php
      $studentNames = explode(',', $value['studentName']);
      $studentMarks = explode(',', $value['studentMarks']);
    ?>
    @if(!empty($studentNames))
      @foreach($studentNames as $skey => $svalue)
        <tr>
          <td>{{$value['title']}}</td>
          <td>{{$value['assignmentDate']}}</td>
          <td>{{$value['name']}}</td>
          <td>{{@$svalue}}</td>
          <td>{{@$studentMarks[$skey]}}</td>
        </tr>
      @endforeach
    @endif
  @endforeach
@endif
</tbody>









                  
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
