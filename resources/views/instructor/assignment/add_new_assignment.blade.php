<!-- Created By Badar
date 17/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Course: {{@$result[0]['courseName']}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/save-assignment') }}" id="assignmentRegistration" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <input type="hidden" name="courseID" value="{{ $result[0]['id'] }}" />
        <div class="card-body">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" id="title" placeholder="Enter Title" required
                    name="title" />
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" id="description" value="" placeholder="Enter Detail"
                    name="description"></textarea>
            </div>
            <div class="form-group">
                <label>Deadline Date</label>
                <input type="date" class="form-control" id="deadline" placeholder="Enter deadline to submit assignment" required
                    name="deadline" />
            </div>
            <div class="form-group">
                <label>Deadline Date</label>
                <input type="time" class="form-control" id="deadline Time" placeholder="Enter deadline Time to submit assignment" required
                    name="deadlineTime" />
            </div>
            <div class="form-group">
                <label>File</label>     
                <input type="file" required multiple class="form-control" id="assignmentFile" name="file[]" />                   
            </div> 
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var form = $('#assignmentRegistration')[0];
        var formData = new FormData(form);
    $.ajax({
        type: "POST",
        url: site_url + "/save-assignment",
        data:  formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}

</script>