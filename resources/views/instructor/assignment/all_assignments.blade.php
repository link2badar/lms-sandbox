@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Assignments</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Assignment Date</th>
                    <th>Title</th>
                    <th>Description</th>
                     <th>Deadline</th>
                    <th>File</th>
                    <th>Course</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{date("d/M/Y h:i:s A", strtotime($value['created_at']))}}</td>
                    <td>{{$value['title']}}</td>
                    <td>{{$value['description']}}</td>
                    <td>{{date("d/M/Y h:i A", strtotime($value['deadline']))}}</td>
                    <td>@if($value['file'] != NULL)<a href="{{ url('/download-assignment/'.$value['file']) }}">download <i class="fa fa-download"></i></a>@endif</td>
                    <td>{{$value['courseName']}}</td>
                    <td>
                    <?php $gradeFound = false;
                      foreach($gradeFilter as $gkey => $gvalue){
                        if($gvalue['assignmentID']  ==  $value['id']) {$gradeFound = true; break;}
                      }
                    ?>
                    @if($gradeFound) Grade Added @else
                      <button type="button" class="btn bg-gradient-info btn-xs" onclick="loadModal('/assignment-grade/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Add Grade</button>
                        |
                        @endif
                    <a href="{{ url('/submission-list/'.$value['id']) }}"><button type="button" class="btn bg-gradient-info btn-xs">Submission List</button></a>
                        |
                    <button type="button" class="btn bg-gradient-primary btn-xs" onclick="loadModal('/edit-assignment/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Edit</button>
 |
                        
                        <a href="{{ url('/delete-assignment/'.$value['id']) }}"><button type="button" class="btn bg-gradient-danger btn-xs">Delete</button></a>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
