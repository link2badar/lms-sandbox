<!-- Created By Badar
date 17/8/2020 -->

<div class="modal-header">
    <h4 class="modal-title">Exam: @if(@$studentList[0]['title'] == '') {{@$studentListByGroup[0]['title']}} @else {{@$studentList[0]['title']}} @endif</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/save-exam-grade') }}" id="examGrade" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <input type="hidden" name="examID" value="@if(@$studentList[0]['id'] == '') {{@$studentListByGroup[0]['id']}} @else {{@$studentList[0]['id']}} @endif" />
        <div class="card-body">
            <div class="form-group">
                <label>exam Delivered Date</label>
                <input type="text" class="form-control" id="title" readonly="readonly" placeholder="Enter Date" required
                    name="examDate" value="@if(@$studentList[0]['created_at'] == ''){{date('d/m/Y',strtotime(@$studentListByGroup[0]['created_at']))}}@else{{date('d/m/Y',strtotime(@$studentList[0]['created_at']))}}@endif" />
            </div>
            <div class="form-group">
                <label>Student List</label>
                <table class="table">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Marks</th>
                    </tr>
                    <?php 
                    if(!empty($studentList)){
                    $counter = 1;
                    foreach ($studentList as $key => $value) {
                        $studentDataID = explode(",", $value['studentID']);
                        $studentDataEmail = explode(",", $value['email']);
                        $studentDataName = explode(",", $value['studentName']);
                        foreach ($studentDataName as $skey => $svalue) {?>
                        <tr>
                            <td>{{$counter++}}</td>
                            <td>{{$studentDataName[$skey]}}<input type="hidden" value="{{$studentDataID[$skey]}}" name="studentID[]" /></td>
                            <td>{{$studentDataEmail[$skey]}}</td>
                            <td><input type="number" min="0" name="studentMarks[]" value="0" /></td>
                        </tr>
                        <?php }
                    }
                }

                if(!empty($studentListByGroup)){
                    $counter = 1;
                    foreach ($studentListByGroup as $key => $value) {
                        $studentDataID = explode(",", $value['sID']);
                        $studentDataEmail = explode(",", $value['email']);
                        $studentDataName = explode(",", $value['studentName']);
                        foreach ($studentDataName as $skey => $svalue) {?>
                        <tr>
                            <td>{{$counter++}}</td>
                            <td>{{$studentDataName[$skey]}}<input type="hidden" value="{{$studentDataID[$skey]}}" name="studentID[]" /></td>
                            <td>{{$studentDataEmail[$skey]}}</td>
                            <td><input type="number" min="0" name="studentMarks[]" value="0" /></td>
                        </tr>
                        <?php }
                    }
                }
                    ?>
                    
                </table>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var form = $('#examGrade')[0];
        var formData = new FormData(form);
    $.ajax({
        type: "POST",
        url: site_url + "/save-exam-grade",
        data:  formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}

</script>