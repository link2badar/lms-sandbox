@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Exams</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">

                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Exam Date</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Remaining Time (HH:MM)</th>
                    <th>File</th>
                    <th>Course</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 0; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr><?php 
                    if(strtotime($value['deadline']) > strtotime(date("Y-m-d H:i:s")))
                      $timer =  date("H:i" , strtotime($value['deadline']) - strtotime(date("Y-m-d H:i:s")));
                      else{
                        $timer = '00:00';
                      }
                  
                  ?>
                    <td>{{date("d/M/Y h:i:s A", strtotime($value['created_at']))}}</td>
                    <td>{{$value['title']}}</td>
                    <td>{{$value['description']}}</td>
                    <td>{{$timer}}</td>
                    <td>@if($value['file'] != NULL)<a href="{{ url('/download-assignment/'.$value['file']) }}">download <i class="fa fa-download"></i></a>@endif</td>
                    <td>{{$value['courseName']}}</td>
                    <td>
                    <?php $gradeFound = false;
                      foreach($gradeFilter as $gkey => $gvalue){
                        if($gvalue['examID']  ==  $value['id']) {$gradeFound = true; break;}
                      }
                    ?>
                    @if($gradeFound) Grade Added @else
                      <button type="button" class="btn bg-gradient-info btn-xs" onclick="loadModal('/exam-grade/{{$value['id'] }}')" data-toggle="modal" data-target="#modal-lg">Add Grade</button>
                        |
                        @endif
                    <a href="{{ url('/submission-exam-list/'.$value['id']) }}"><button type="button" class="btn bg-gradient-info btn-xs">Submission List</button></a>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
