@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Deadline: {{ date("d/M/Y h:i:s A", strtotime(@$result[0]['deadline'])) }}</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Submission Date</th>
                     <th>Student Name</th>
                     <th>Description</th>
                    <th>File</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($result))
                      @foreach($result as $key => $value)
                  <tr>
                    <td <?php if(strtotime($value['deadline']) < strtotime($value['created_at'])) echo 'style="background:red"'; ?>>{{$counter++}}</td>
                    <td>{{date("d/M/Y h:i:s A", strtotime($value['created_at']))}}</td>
                    <td>{{$value['name']}}</td>
                    <td>{{$value['description']}}</td>
                    <td>@if($value['file'] != NULL)<a href="{{ url('/download-assignment/'.$value['file']) }}">download <i class="fa fa-download"></i></a>@endif</td>
                  </tr>
                  @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
