<!-- Created By Badar
date 23/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Register New Issue</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/submit-instructor-help') }}" id="submitInstructorHelp" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
        <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" id="subject" value="" placeholder="Enter Subject"
                    name="subject"></textarea>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" id="description" value="" placeholder="Enter Detail"
                    name="description"></textarea>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var form = $('#submitInstructorHelp')[0];
        var formData = new FormData(form);
    $.ajax({
        type: "POST",
        url: site_url + "/submit-instructor-help",
        data:  formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}

</script>