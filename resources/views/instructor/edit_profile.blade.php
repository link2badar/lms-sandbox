@extends('layouts.adminapp')
@section('content')
<div class="container-fluid">  
@if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif    
    <div class="row">       
    
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-danger" style="display:none"></div>
            <form role="form" method="post" action="{{ url('/update-instructor-profile') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="instructorfName">First Name</label>
                        <input type="text" class="form-control" id="instructorfName" placeholder="Enter First Name" required
                            name="instructorfName" value="{{ @$userData[0]['instructorfName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="instructormName">Middle Name</label>
                        <input type="text" class="form-control" id="instructormName" placeholder="Enter Middle Name"
                            name="instructormName" value="{{ @$userData[0]['instructormName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="instructorlName">Last Name</label>
                        <input type="text" class="form-control" id="instructorlName" placeholder="Enter Last Name" required
                            name="instructorlName" value="{{ @$userData[0]['instructorlName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter Email"
                            name="email" value="{{ @$userData[0]['email']}}" readonly="readonly"  />
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter Password"
                            name="instructorPass" value="">
                            <span class="help-desk">Leave password blank, if you don't want to update</span>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone #</label>
                        <input type="number" class="form-control" id="phone" placeholder="Enter Phone #"
                            name="instructorPhone" value="{{ @$userData[0]['instructorPhone']}}" />
                    </div>
                    <div class="form-group">
                        <label for="address">Complete Address</label>
                        <textarea class="form-control" id="address" placeholder="Enter Address"
                        name="instructorAddress">{{ @$userData[0]['instructorAddress']}}</textarea>
                    </div>
                    <hr />
                </div>
                <!-- /.card-body -->
                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>       
    </div>
</div>        

@endsection
