@extends('layouts.adminapp')
@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Video Lectures</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              <video width="100%" controls>
              <source src="{{ asset('public/uploadedFile/'.$file) }}" type="video/mp4">
              <source src="{{ asset('public/uploadedFile/'.$file) }}" type="video/webm" />
              </video>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
