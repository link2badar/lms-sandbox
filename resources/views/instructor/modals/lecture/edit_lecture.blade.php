<!-- Created By Badar
date 17/8/2020 -->
<div class="modal-header">
    <h4 class="modal-title">Update Lecture: {{@$result[0]['title']}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/update-lecture/'.$result[0]['id']) }}" id="lectureRegistrationUpdate" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label>Title</label>
                <input type="text" class="form-control" id="title" placeholder="Enter Title" required
                    name="title" value="{{$result[0]['title']}}"/>
            </div>
            <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" id="description" value="" placeholder="Enter Detail"
                    name="description">{{$result[0]['description']}}</textarea>
            </div>
            <div class="form-group">
                <label>File</label>     
                <input type="file" required multiple class="form-control" id="lectureFile" name="file[]" />
                <span class='help-desk'>Leave it, if you don't want to update</span>                  
            </div> 
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    var form = $('#lectureRegistrationUpdate')[0];
        var formData = new FormData(form);
        var id = "{{$result[0]['id']}}";
    $.ajax({
        type: "POST",
        url: site_url + "/update-lecture/"+id,
        data:  formData,
        dataType: 'json',
        processData: false,
        contentType: false,
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}

</script>