@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">Quiz  Grading System</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Quiz Title</th>
                        <th>Quiz Date</th>
                        <th>Student Marks</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($result))
                    @foreach($result as $key => $value)
                        <?php
                        $studentID      = explode(',', $value['studentID']);
                        $studentMarks   = explode(',', $value['studentMarks']);
                        ?>
                        @if(!empty($studentID))
                        @foreach($studentID as $skey => $svalue)
                        <?php if($svalue != $sID) continue; ?>
                            
                            <tr>
                            <td>{{$value['title']}}</td>
                            <td>{{$value['quizDate']}}</td>
                            <td>{{@$studentMarks[$skey]}}</td>
                            </tr>
                        @endforeach
                        @endif
                    @endforeach
                    @endif
                    </tbody>
                    <tfoot>
            <tr>
                <th colspan="2" style="text-align:right">Total:</th>
                <th></th>
            </tr>
        </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
