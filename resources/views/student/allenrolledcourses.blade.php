@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <div class="row">
          <div class="col-12">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Courses</h3>
              </div>
              @if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                      <?php $counter = 1; ?>
                      @if(!empty($assignedCourses))
                      @foreach($assignedCourses as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$value['courseName']}}</td>
                    <td><a href="{{ url('/all-student-lectures/'.$value['courseID'])}}" class="btn btn-xs btn-primary">Lectures</a> | <a href="{{ url('/all-student-video/'.$value['courseID'])}}" class="btn btn-xs bg-purple">Videos</a> | <a href="{{ url('/all-student-practices/'.$value['courseID'])}}" class="btn btn-xs btn-success">Practice Material</a> | <a href="{{ url('/all-student-assignments/'.$value['courseID'])}}" class="btn btn-xs btn-info">Assignments</a> | <a href="{{ url('/all-student-quizzes/'.$value['courseID'])}}" class="btn btn-xs btn-secondary">Quiz</a> | <a href="{{ url('/all-student-exams/'.$value['courseID'])}}" class="btn bg-gradient-primary btn-xs">Exam</a></td>
                  </tr>
                  @endforeach
                  @endif
                      @if(!empty($assignedCoursesByGroup))
                      @foreach($assignedCoursesByGroup as $key => $value)
                  <tr>
                    <td>{{$counter++}}</td>
                    <td>{{$value['courseName']}}</td>
                    <td><a href="{{ url('/all-student-lectures/'.$value['courseID'])}}" class="btn btn-xs btn-primary">Lectures</a> | <a href="{{ url('/all-student-video/'.$value['courseID'])}}" class="btn btn-xs bg-purple">Videos</a> | <a href="{{ url('/all-student-practices/'.$value['courseID'])}}" class="btn btn-xs btn-success">Practice Material</a> | <a href="{{ url('/all-student-assignments/'.$value['courseID'])}}" class="btn btn-xs btn-info">Assignments</a> | <a href="{{ url('/all-student-quizzes/'.$value['courseID'])}}" class="btn btn-xs btn-secondary">Quiz</a>| <a href="{{ url('/all-student-exams/'.$value['courseID'])}}" class="btn bg-gradient-primary btn-xs">Exam</a></td>
                  </tr>
                  @endforeach
                  @endif
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
@endsection
