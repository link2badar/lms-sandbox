@extends('layouts.adminapp')
@section('content')
<div class="container-fluid">  
@if(Session::has('message'))
              <div class="alert alert-<?php if(@Session::get('danger') == 'true') echo 'danger'; else echo 'success'; ?> alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                {{Session::get('message')}}
              </div>
              @endif    
    <div class="row">       
    
        <div class="col-lg-12 col-md-12">
            <div class="alert alert-danger" style="display:none"></div>
            <form role="form" method="post" action="{{ url('/update-student-profile') }}">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="studentfName">First Name</label>
                        <input type="text" class="form-control" id="studentfName" placeholder="Enter First Name" required
                            name="studentfName" value="{{ @$userData[0]['studentfName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="studentmName">Middle Name</label>
                        <input type="text" class="form-control" id="studentmName" placeholder="Enter Middle Name"
                            name="studentmName" value="{{ @$userData[0]['studentmName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="studentlName">Last Name</label>
                        <input type="text" class="form-control" id="studentlName" placeholder="Enter Last Name" required
                            name="studentlName" value="{{ @$userData[0]['studentlName']}}" >
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" placeholder="Enter Email"
                            name="email" value="{{ @$userData[0]['email']}}" readonly="readonly"  />
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" placeholder="Enter Password"
                            name="studentPass" value="">
                            <span class="help-desk">Leave password blank, if you don't want to update</span>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone #</label>
                        <input type="number" class="form-control" id="phone" placeholder="Enter Phone #"
                            name="studentPhone" value="{{ @$userData[0]['studentPhone']}}" />
                    </div>
                    <div class="form-group">
                        <label for="address">Complete Address</label>
                        <textarea class="form-control" id="address" placeholder="Enter Address"
                        name="studentAddress">{{ @$userData[0]['studentAddress']}}</textarea>
                    </div>
                    <hr />
                </div>
                <!-- /.card-body -->
                <button type="submit" class="btn btn-primary">Save changes</button>
            </form>
        </div>       
    </div>
</div>        

@endsection
