@extends('layouts.adminapp')

@section('content')
<script>
function formSubmit() {
    if ($(".msgDetail").val() == "") {
        alert("You can't send empty message");
        return false;
    }
    $.ajax({
        type: "POST",
        url: site_url + "/save-chat-detail",
        data: $(".sendHelpMsg").serialize(),
        dataType: "json",
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        success: function (result) {
        
            var appendMsg = '<div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+result.senderName+'</span><span class="direct-chat-timestamp float-left">'+result.senderDate+'</span></div><div class="direct-chat-text">'+result.description+'</div></div>';
            $(".senderSide").append(appendMsg);
            $(".msgDetail").val('');
            $(".direct-chat-messages").animate({
    scrollTop: $(".direct-chat-messages").get(0).scrollHeight,
});
            }
    });
}

setInterval(ajaxCall, 3000); //300000 MS == 5 minutes
var authID = '{{Auth::user()->id}}';
function ajaxCall() {
  $.ajax({
    url: site_url + "/chat-intervalChat/{{@$instructorName[0]['id']}}",
    dataType: "json",
    headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
    },
    success: function (result) {
      var reloadContent = '';
      $.each(result, function(key, ovalue) {
          $.each(ovalue, function(ikey, value) {           
          if(value['sendBy'] != authID){
            reloadContent += '<div class="direct-chat-msg"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-left">'+value['senderName']+'</span><span class="direct-chat-timestamp float-right">'+value['created_at']+'</span></div><div class="direct-chat-text">'+value['description']+'</div></div>';
          }
          else{
            reloadContent   +=  '<div class="senderSide"><div class="direct-chat-msg right"><div class="direct-chat-infos clearfix"><span class="direct-chat-name float-right">'+value['senderName']+'</span><span class="direct-chat-timestamp float-left">'+value['created_at']+'</span></div><div class="direct-chat-text">'+value['description']+'</div></div></div>';
          }
        });
      });
      $("#msgList").html(reloadContent);
    }
  });
}

</script>
<div class="container-fluid">
        <div class="row">
        <div class="col-md-3">
            <a href="#" class="btn btn-primary btn-block mb-3">Instructor List</a>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">All Instructors</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body p-0">
                <ul class="nav nav-pills flex-column">
                @if(!empty($instructor))
                @foreach($instructor as $ikey => $value)
                  <li class="nav-item">
                    <a href="{{ url('student-chat/'.$value['id']) }}" class="nav-link">
                      <i class="fas fa-user"></i> {{@$value['instructorfName'].' '.@$value['instructormName'].' '.@$value['instructorlName']}}
                      
                    </a>
                  </li>
                  @endforeach
                  @endif
                  @if(!empty($instructorByGroup))
                @foreach($instructorByGroup as $ikey => $value)
                  <li class="nav-item">
                    <a href="{{ url('student-chat/'.$value['id']) }}" class="nav-link">
                      <i class="fas fa-user"></i> {{@$value['instructorfName'].' '.@$value['instructormName'].' '.@$value['instructorlName']}}
                      
                    </a>
                  </li>
                  @endforeach
                  @endif
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->  






          <div class="col-md-9">
          <div class="card">

<div class="card card-info direct-chat direct-chat-info" style="height: 425px;">
  <div class="card-header">
    <h3 class="card-title">{{@$instructorName[0]['instructorfName'].' '.@$instructorName[0]['instructormName'].' '.@$instructorName[0]['instructorlName']}}</h3><br />
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages" id="msgList">

        @if(!empty($result))
        @foreach($result as $key => $value)

        @if(@$value['sendBy'] != Auth::user()->id)
      <!-- Message. Default to the left -->
      <div class="direct-chat-msg">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-left">{{@$value['senderName']}}</span>
          <span class="direct-chat-timestamp float-right">{{@date("y-m-d h:i A", strtotime(@$value['created_at'])) }} </span>
        </div>
        <div class="direct-chat-text">
        {{@$value['description']}}
        </div>
      </div>
      @else
      <!-- Message to the right -->
      <div class="senderSide">
      <div class="direct-chat-msg right">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-right">{{@$value['senderName']}}</span>
          <span class="direct-chat-timestamp float-left">{{@date("y-m-d h:i A", strtotime($value['created_at']))}}</span>
        </div>
        <div class="direct-chat-text">
          {{@$value['description']}}
        </div>
      </div>
      </div>
      @endif

      @endforeach
      @endif
      <!-- /.direct-chat-msg -->
    </div>
    <!--/.direct-chat-messages-->

  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <form action="#" method="post" class="sendHelpMsg">
    <input type="hidden" name="sendTo" value="{{@$instructorName[0]['id']}}" />
    <input type="hidden" name="sendBy" value="{{ Auth::user()->id}}" />
    <input type="hidden" name="senderName" value="{{ Auth::user()->name}}" />
    <input type="hidden" name="receiverName" value="{{@$instructorName[0]['instructorfName'].' '.@$instructorName[0]['instructormName'].' '.@$instructorName[0]['instructorlName']}}" />
    
      <div class="input-group">
        <textarea name="description" placeholder="Type Message ..." class="form-control msgDetail"></textarea>
        <span class="input-group-append">
          <button type="button" class="btn btn-primary" onclick="formSubmit()">Send</button>
        </span>
      </div>
    </form>
  </div>
  <!-- /.card-footer-->
</div>
<!--/.direct-chat -->
</div>
</div>
</div>
</div>

<script>

                </script>
@endsection
