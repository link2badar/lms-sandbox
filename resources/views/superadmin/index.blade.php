@extends('layouts.adminapp')

@section('content')
<div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        @if(Auth::user()->role == 0)
        <div class="row">          
          <div class="col-lg-6 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($result)}}</h3>
                <p>Total Schools Registered</p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ @count($instructors)}}</h3>
                <p>Total Instructors Registered</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ @count($students)}}</h3>
                <p>Total Student Registered</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
            </div>
          </div>
        </div>
        @endif
        @if(Auth::user()->role == 1)
        <div class="row">          
          <div class="col-lg-6 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($schoolClasses)}}</h3>
                <p>Total Classes Registered</p>
              </div>
              <div class="icon">
                <i class="ion ion-home"></i>
              </div>
              <a href="{{ url('all-groups') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-6">
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ @count($schoolInstructors)}}</h3>
                <p>Total Instructors Registered</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
              <a href="{{ url('all-instructors') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-6 col-6">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ @count($students)}}</h3>
                <p>Total Student Registered</p>
              </div>
              <div class="icon">
                <i class="fas fa-users"></i>
              </div>
            </div>
          </div>
        </div>
        @endif
        @if(Auth::user()->role == 2)
        <div class="row">          
          <div class="col-lg-6 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($courses)}}</h3>
                <p>Total Courses Assigned</p>
              </div>
              <div class="icon">
                <i class="fa fa-book"></i>
              </div>
              <a href="{{ url('all-assigned-courses') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        @endif
        @if(Auth::user()->role == 3)
        <div class="row">          
          <div class="col-lg-6 col-6">
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{ @count($courses)+@count($coursesByGroup)}}</h3>
                <p>Total Enrolled Courses</p>
              </div>
              <div class="icon">
                <i class="fa fa-book"></i>
              </div>
              <a href="{{ url('all-enrolled-courses') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
        </div>
        @endif
        <!-- /.row -->
        <!-- Main row -->
      
      </div><!-- /.container-fluid -->
@endsection
