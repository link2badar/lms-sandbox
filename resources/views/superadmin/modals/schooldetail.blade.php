<div class="modal-header">
    <h4 class="modal-title">School: {{ $result[0]['schoolName'] }}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <form role="form" method="post" action="{{ url('/save-new-school') }}" id="schoolRegistration">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="schoolName">School Name</label>
                <input type="text" class="form-control" id="schoolName" placeholder="Enter school Name" required
                    name="schoolName" disabled="disabled" value="{{ $result[0]['schoolName'] }}" />
            </div>
            <div class="form-group">
                <label for="adminName">Admin full Name</label>
                <input type="text" class="form-control" id="adminName" placeholder="Enter School Admin Name" required
                    name="adminName" disabled="disabled" value="{{ $result[0]['adminName'] }}" >
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter School Admin Email" required
                    name="email" disabled="disabled" value="{{ $result[0]['email'] }}">
            </div>
           
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="text" class="form-control" value="" id="phone" placeholder="Enter Phone #"
                    name="schoolPhone"  disabled="disabled" value="{{ $result[0]['schoolPhone'] }}"/>
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="schoolAddress"  disabled="disabled">{{ $result[0]['schoolAddress'] }}</textarea>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Classes">Total Classes</label>
                        <input type="number" class="form-control" id="Classes" placeholder="Enter Classes" name="totalClasses"
                            required disabled="disabled" value="{{ $result[0]['totalClasses'] }}" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Students">Total Students</label>
                        <input type="number" class="form-control" id="Students" placeholder="Enter Students" name="totalStudents"
                            required disabled="disabled" value="{{ $result[0]['totalStudents'] }}"></textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
