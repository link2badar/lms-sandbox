<div class="modal-header">
    <h4 class="modal-title">Register New School</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <div class="alert alert-danger" style="display:none"></div>
    <form role="form" method="post" action="{{ url('/save-new-school') }}" id="schoolRegistration">
        @csrf
        <input type="hidden" name="addedBy" value="{{ Auth::user()->id }}" />
        <div class="card-body">
            <div class="form-group">
                <label for="schoolName">School Name</label>
                <input type="text" class="form-control" id="schoolName" placeholder="Enter school Name" required
                    name="schoolName" />
            </div>
            <div class="form-group">
                <label for="adminName">Admin full Name</label>
                <input type="text" class="form-control" id="adminName" placeholder="Enter School Admin Name" required
                    name="adminName" >
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" placeholder="Enter School Admin Email" required
                    name="email">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="password" placeholder="Enter School Admin Password"
                    required name="schoolPass">
            </div>
            <div class="form-group">
                <label for="phone">Phone #</label>
                <input type="number" class="form-control" value="" id="phone" placeholder="Enter Phone #"
                    name="schoolPhone" />
            </div>
            <div class="form-group">
                <label for="address">Complete Address</label>
                <textarea class="form-control" id="address" value="" placeholder="Enter Address"
                    name="schoolAddress"></textarea>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Classes">Total Classes</label>
                        <textarea class="form-control" id="Classes" placeholder="Enter Classes" name="totalClasses"
                            required></textarea>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="Students">Total Students</label>
                        <textarea class="form-control" id="Students" placeholder="Enter Students" name="totalStudents"
                            required></textarea>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </form>
</div>
<div class="alert alert-danger" style="display:none"></div>
<div class="modal-footer justify-content-between">

    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="submit" class="btn btn-primary" onclick="submitForm()">Save changes</button>
</div>

<script>
function submitForm() {
    $.ajax({
        type: "POST",
        url: site_url + "/save-new-school",
        data:  $("#schoolRegistration").serialize(),
        dataType: 'json',
        success: function(result) {
            if (result.errors) {
                $('.alert-danger').html('');

                $.each(result.errors, function(key, value) {
                    $('.alert-danger').show();
                    $('.alert-danger').append('<li>' + value + '</li>');
                });
            } else {
                
                $('.alert-danger').hide();
                $('#modal-lg').modal('hide');
                location.reload(true);
            }
        }
    });
}
</script>