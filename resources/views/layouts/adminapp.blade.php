<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>LMS | Dashboard</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="{{ asset('public/admin/font/all.min.css') }}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/responsive.bootstrap4.min.css') }}">

  <!-- Modals -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/toastr.min.css') }}">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/bootstrap-4.min.css') }}">

  <link rel="stylesheet" href="{{ asset('public/admin/css/icheck-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/jqvmap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/adminlte.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/OverlayScrollbars.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/daterangepicker.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('public/admin/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('public/admin/css/summernote-bs4.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.css" rel="stylesheet"/>
  <link href="{{ asset('public/admin/css/media.css')}}" />

  <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  <script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en'}, 'google_translate_element');
}
</script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <div id="google_translate_element">
            </div>
            <ul class="dropdown-menu">
              <!-- Menu Body -->
              <!-- Menu Footer-->
            </ul>
          </li>
        </ul>
      </div>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('dashboard') }}" class="brand-link">
      <span class="brand-text font-weight-light">Dashboard Control</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('public/admin/img/avatar3.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{Auth::user()->name}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
        <!--  <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="./index.html" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v1</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index2.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v2</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="./index3.html" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Dashboard v3</p>
                </a>
              </li>
            </ul>
          </li> -->
          
          <li class="nav-item">
            <a href="{{ url('dashboard') }}" class="nav-link {{ ($pageName == 'Dashboard') ? 'active' : '' }}">
              <i class="nav-icon fas fa-globe"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          @if(Auth::user()->role == 0)
          <li class="nav-item">
            <a href="{{ url('all-schools') }}" class="nav-link {{ ($pageName == 'All Schools') ? 'active' : '' }}">
              <i class="nav-icon fas fa-school"></i>
              <p>
                All Schools
              </p>
            </a>
          </li>
          @endif

          @if(Auth::user()->role == 1)
          <li class="nav-item">
            <a href="{{ url('all-instructors') }}" class="nav-link {{ ($pageName == 'All Instructors') ? 'active' : '' }}">
            <i class="fas fa-users"></i>
              <p>
                All Instructors
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-groups') }}" class="nav-link {{ ($pageName == 'All Groups') ? 'active' : '' }}">
            <i class="fas fa-archway"></i>
              <p>
                All Groups
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-courses') }}" class="nav-link {{ ($pageName == 'All Courses') ? 'active' : '' }}">
            <i class="fas fa-book"></i>
              <p>
                All Courses
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-students') }}" class="nav-link {{ ($pageName == 'All Students') ? 'active' : '' }}">
            <i class="fas fa-users"></i>
              <p>
                All Students
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-assign') }}" class="nav-link {{ ($pageName == 'All Assigns') ? 'active' : '' }}">
            <i class="fas fa-users"></i>
              <p>
                Assign Instructor
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-{{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade') ? 'open' : 'close' }}">
            <a href="#" class="nav-link  {{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                View Grade
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('school-lecture-grade') }}" class="nav-link  {{ ($pageName == 'All Lecture Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lecture</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('school-assignment-grade') }}" class="nav-link  {{ ($pageName == 'All Assignment Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assignment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('school-material-grade') }}" class="nav-link  {{ ($pageName == 'All Material Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Practice Material</p>
                </a>
              </li>
              </ul>
              </li>

              <li class="nav-item has-treeview menu-{{ ($pageName == 'All Lectures') || ($pageName == 'All Assignments') || ($pageName == 'All Materials') ? 'open' : 'close' }}">
            <a href="#" class="nav-link  {{ ($pageName == 'All Lectures') || ($pageName == 'All Assignments') || ($pageName == 'All Materials') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                View Instructor Content
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('school-lectures') }}" class="nav-link  {{ ($pageName == 'All Lectures') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lecture</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('school-assignments') }}" class="nav-link  {{ ($pageName == 'All Assignments') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assignment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('school-material') }}" class="nav-link  {{ ($pageName == 'All Materials') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Practice Material</p>
                </a>
              </li>
              </ul>
              </li>
              <li class="nav-item">
            <a href="{{ url('school-help') }}" class="nav-link {{ ($pageName == 'Help Desk' || $pageName == 'Help Discussion') ? 'active' : '' }}">
            <i class="fas fa-handshake"></i>
              <p>
                Student Help
              </p>
            </a>
          </li>

           <li class="nav-item">
            <a href="{{ url('school-instuctor-help') }}" class="nav-link {{ ($pageName == 'Instructor Help Desk' || $pageName == 'Instructor Help Discussion') ? 'active' : '' }}">
            <i class="fas fa-handshake"></i>
              <p>
                Instructor Help
              </p>
            </a>
          </li>
          @endif
          @if(Auth::user()->role == 2)
          <li class="nav-item">
            <a href="{{ url('all-assigned-courses') }}" class="nav-link {{ ($pageName == 'All Assigned Courses') ? 'active' : '' }}">
            <i class="fas fa-book"></i>
              <p>
                Assigned Courses
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-lectures') }}" class="nav-link {{ ($pageName == 'All Lectures') ? 'active' : '' }}">
            <i class="fas fa-newspaper-o"></i>
              <p>
                All Lectures
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{ url('all-videos') }}" class="nav-link {{ ($pageName == 'Video Lectures') ? 'active' : '' }}">
            <i class="fas fa-newspaper-o"></i>
              <p>
                Video Lectures
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-assignments') }}" class="nav-link {{ ($pageName == 'All Assignments') ? 'active' : '' }}">
            <i class="fas fa-newspaper-o"></i>
              <p>
                All Assignments
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ url('all-material') }}" class="nav-link {{ ($pageName == 'All Assignments') ? 'active' : '' }}">
            <i class="fas fa-newspaper-o"></i>
              <p>
                Practice Material
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('all-quizzes') }}" class="nav-link {{ ($pageName == 'All Quizzes') ? 'active' : '' }}" title="contact with admin">
            <i class="fas fa-comment"></i>
              <p>
                Quizzes
              </p>
            </a>
          </li>
           <li class="nav-item">
            <a href="{{ url('all-exams') }}" class="nav-link {{ ($pageName == 'All Exams') ? 'active' : '' }}" title="contact with admin">
            <i class="fas fa-file-word"></i>
              <p>
                Exams
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-{{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade')  || ($pageName == 'All Quiz Grades') || ($pageName == 'All Exam Grades') ? 'open' : 'close' }}">
            <a href="#" class="nav-link  {{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade') || ($pageName == 'All Quiz Grades') || ($pageName == 'All Exam Grades') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                View Grade
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('all-lecture-grade') }}" class="nav-link  {{ ($pageName == 'All Lecture Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lecture</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('all-assignment-grade') }}" class="nav-link  {{ ($pageName == 'All Assignment Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assignment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('all-material-grade') }}" class="nav-link  {{ ($pageName == 'All Material Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Practice Material</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('instructor-quiz-grade') }}" class="nav-link  {{ ($pageName == 'All Quiz Grades') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Quizzes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('instructor-exam-grade') }}" class="nav-link  {{ ($pageName == 'All Exam Grades') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Exams</p>
                </a>
              </li>
              </ul>
              </li>
             
            <li class="nav-item">
            <a href="{{ url('instructor-help') }}" class="nav-link {{ ($pageName == 'Help Desk' || $pageName == 'Help Discussion') ? 'active' : '' }}">
            <i class="fas fa-handshake"></i>
              <p>
               Help
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ url('instructor-chat') }}" class="nav-link {{ ($pageName == 'Chat Box') ? 'active' : '' }}" title="contact with admin">
            <i class="fas fa-comment"></i>
              <p>
                Chat
              </p>
            </a>
          </li>

          @endif
          @if(Auth::user()->role == 3)
          <li class="nav-item">
            <a href="{{ url('all-enrolled-courses') }}" class="nav-link {{ ($pageName == 'All Enrolled Courses') ? 'active' : '' }}">
            <i class="fas fa-newspaper"></i>
              <p>
                Enrolled Courses
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview menu-{{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade')  || ($pageName == 'All Quiz Grades') || ($pageName == 'All Exam Grades') ? 'open' : 'close' }}">
            <a href="#" class="nav-link  {{ ($pageName == 'All Lecture Grade') || ($pageName == 'All Assignment Grade') || ($pageName == 'All Material Grade')  || ($pageName == 'All Quiz Grades') || ($pageName == 'All Exam Grades') ? 'active' : '' }}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                View Grade
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('student-lecture-grade') }}" class="nav-link  {{ ($pageName == 'All Lecture Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Lecture</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('student-assignment-grade') }}" class="nav-link  {{ ($pageName == 'All Assignment Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Assignment</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('student-material-grade') }}" class="nav-link  {{ ($pageName == 'All Material Grade') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Practice Material</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('student-quiz-grade') }}" class="nav-link  {{ ($pageName == 'All Quiz Grades') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Quizzes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ url('student-exam-grade') }}" class="nav-link  {{ ($pageName == 'All Exam Grades') ? 'active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Exams</p>
                </a>
              </li>
              </ul>
              </li>
              <li class="nav-item">
            <a href="{{ url('student-chat') }}" class="nav-link {{ ($pageName == 'Chat Box') ? 'active' : '' }}" title="contact with admin">
            <i class="fas fa-comment"></i>
              <p>
                Chat
              </p>
            </a>
          </li>
              <li class="nav-item">
            <a href="{{ url('student-help') }}" title="chat with instructors" class="nav-link {{ ($pageName == 'Help Desk' || $pageName == 'Help Discussion') ? 'active' : '' }}">
            <i class="fas fa-handshake"></i>
              <p>
                Help
              </p>
            </a>
          </li>
          @endif
          <li class="nav-item">
            <a href="{{ url('profile') }}" class="nav-link {{ ($pageName == 'View/Edit Profile') ? 'active' : '' }}">
            <i class="fas fa-edit"></i>
              <p>
                Profile
              </p>
            </a>
          </li>
          
          
          <li class="nav-item">
            <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="nav-link">
              <i class="fa fa-sign-out nav-item" style="font-size: 1.1rem;"></i>
              <p>
                Logout
              </p>
              <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $pageName }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            @if(!empty($breadcrumbs))
            @foreach($breadcrumbs as $key => $val)
              <li class="breadcrumb-item"><a href="{{ url($key) }}">{{$val}}</a></li>
              @endforeach
              @endif
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      @yield('content')
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2020-2021 <a href="{{ url('/')}}">LMS</a>.</strong>
    All rights reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

<script src="{{ asset('public/admin/js/jquery.min.js') }}"></script>
<script src="{{ asset('public/admin/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery-ui.min.js') }}"></script>
<script>
  var site_url = "{{ url('/') }}";
  $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('public/admin/js/Chart.min.js') }}"></script>
<script src="{{ asset('public/admin/js/sparkline.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.knob.min.js') }}"></script>

<script src="{{ asset('public/admin/js/moment.min.js') }}"></script>
<script src="{{ asset('public/admin/js/daterangepicker.js') }}"></script>
<script src="{{ asset('public/admin/js/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.overlayScrollbars.min.js') }}"></script>

<!-- SweetAlert2 -->
<script src="{{ asset('public/admin/js/sweetalert2.min.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('public/admin/js/toastr.min.js') }}"></script>

<!-- Select2 -->
<script src="{{ asset('public/admin/js/select2.full.min.js') }}"></script>

<script src="{{ asset('public/admin/js/adminlte.min.js') }}"></script>
<!-- DataTables -->
<script src="{{ asset('public/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/admin/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('public/admin/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('public/admin/js/responsive.bootstrap4.min.js') }}"></script>

<script src="{{ asset('public/admin/js/dashboard.js') }}"></script>
<script src="{{ asset('public/admin/js/demo.js') }}"></script>
<script src="{{ asset('public/admin/js/custom.js') }}"></script>
<script src="{{ asset('public/admin/js/jquery.countdown.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/timepicker.js/latest/timepicker.min.js"></script>

<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
      "scrollX": true,
      footerCallback: function (row, data, start, end, display) {
            var api = this.api(),
                data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === "string"
                    ? i.replace(/[\$,]/g, "") * 1
                    : typeof i === "number"
                    ? i
                    : 0;
            };

            // Total over all pages
            total = api
                .column(2)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(2, { page: "current" })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(2).footer()).html(
                pageTotal
            );
        },
    });
    $('#example2').DataTable({
      "responsive": true,
      "autoWidth": false,
      "scrollX": true,
      footerCallback: function (row, data, start, end, display) {
            var api = this.api(),
                data;

            // Remove the formatting to get integer data for summation
            var intVal = function (i) {
                return typeof i === "string"
                    ? i.replace(/[\$,]/g, "") * 1
                    : typeof i === "number"
                    ? i
                    : 0;
            };

            // Total over all pages
            total = api
                .column(3)
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Total over this page
            pageTotal = api
                .column(3, { page: "current" })
                .data()
                .reduce(function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0);

            // Update footer
            $(api.column(3).footer()).html(
                pageTotal
            );
        },
    });
  });

</script>

</body>
</html>
