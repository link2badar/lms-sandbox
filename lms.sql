-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2020 at 09:25 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lms`
--

-- --------------------------------------------------------

--
-- Table structure for table `assign`
--

CREATE TABLE `assign` (
  `id` int(11) NOT NULL,
  `teacherID` int(11) NOT NULL,
  `courseID` int(11) NOT NULL,
  `studentID` varchar(256) NOT NULL,
  `is_group` tinyint(1) NOT NULL DEFAULT '0',
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assign`
--

INSERT INTO `assign` (`id`, `teacherID`, `courseID`, `studentID`, `is_group`, `addedBy`, `created_at`, `updated_at`) VALUES
(20, 9, 8, '8,9', 0, 23, '2020-08-28 09:40:31', '2020-08-28 09:40:31'),
(21, 10, 9, '9,10', 0, 23, '2020-08-28 09:41:44', '2020-08-28 09:41:44'),
(22, 24, 12, '30,31,32', 0, 23, '2020-08-28 09:42:48', '2020-08-28 09:42:48'),
(23, 12, 14, '13,14', 0, 23, '2020-08-28 09:43:18', '2020-08-28 09:43:18'),
(24, 9, 12, '13,14', 0, 23, '2020-08-28 09:44:02', '2020-08-28 09:44:02'),
(25, 11, 17, '24', 1, 23, '2020-08-28 14:44:57', '2020-08-28 14:44:57');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE `assignment` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext,
  `file` longtext NOT NULL,
  `addedBy` tinyint(10) NOT NULL,
  `courseID` int(11) NOT NULL,
  `deadline` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `title`, `description`, `file`, `addedBy`, `courseID`, `deadline`, `created_at`, `updated_at`) VALUES
(12, 'make calculator', 'solve calculation problem using stack', '', 24, 8, '2020-09-01 03:05:00', '2020-08-28 10:56:45', '2020-08-28 10:56:45'),
(13, 'Assignment Demo', 'demonstration', 'Assignment Demo-67851199320200828150910.zip', 36, 17, '2020-08-31 01:03:00', '2020-08-31 02:09:00', '2020-08-28 15:09:10'),
(14, 'sdfa', 'sdfa', '', 24, 12, '2020-09-01 11:00:00', '2020-08-29 04:58:12', '2020-08-29 04:58:12'),
(15, 'asdf', 'asdf', '', 24, 12, '2020-09-01 02:07:00', '2020-08-29 05:03:17', '2020-08-29 05:03:17'),
(16, 'lk', 'asdf', '', 24, 8, '2020-09-01 14:08:00', '2020-08-29 05:04:07', '2020-08-29 05:04:07'),
(17, 'assignment 1', 'asdf', '', 26, 10, '2021-12-30 12:59:00', '2020-09-01 09:43:01', '2020-09-01 09:43:01'),
(18, 'assignment 2', 'sdf', '', 26, 10, '2020-01-01 01:00:00', '2020-09-01 09:43:47', '2020-09-01 09:43:47'),
(19, 'assignment 3', 'skdk', '', 26, 10, '2020-01-01 01:00:00', '2020-09-01 09:44:11', '2020-09-01 09:44:11');

-- --------------------------------------------------------

--
-- Table structure for table `assignment_grade`
--

CREATE TABLE `assignment_grade` (
  `id` int(11) NOT NULL,
  `assignmentID` int(11) NOT NULL,
  `studentID` varchar(255) NOT NULL,
  `studentMarks` varchar(255) NOT NULL,
  `assignmentDate` varchar(100) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `assignment_grade`
--

INSERT INTO `assignment_grade` (`id`, `assignmentID`, `studentID`, `studentMarks`, `assignmentDate`, `addedBy`, `created_at`, `updated_at`) VALUES
(3, 12, '8,9', '14,17', '28/Aug/2020', 24, '2020-08-28 11:04:27', '2020-08-28 11:04:27');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `sendTo` int(11) NOT NULL,
  `sendBy` int(11) NOT NULL,
  `senderName` varchar(100) NOT NULL,
  `receiverName` varchar(100) NOT NULL,
  `description` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `sendTo`, `sendBy`, `senderName`, `receiverName`, `description`, `created_at`, `updated_at`) VALUES
(1, 11, 31, 'mohid sultan', 'badar munir asif', 'test', '2020-09-03 16:23:10', '2020-09-03 16:23:10'),
(2, 11, 31, 'mohid sultan', 'badar munir asif', 'TESTING', '2020-09-03 16:58:05', '2020-09-03 16:58:05'),
(3, 11, 31, 'mohid sultan', 'badar munir asif', 'okay', '2020-09-03 17:04:06', '2020-09-03 17:04:06'),
(4, 31, 26, 'badar asif', 'mohid sultan', 'instructor here', '2020-09-03 21:20:01', '2020-09-03 21:20:01'),
(5, 11, 31, 'mohid sultan', 'badar munir asif', 'okay sir', '2020-09-03 21:25:17', '2020-09-03 21:25:17'),
(6, 31, 26, 'badar asif', 'mohid sultan', 'how are you?', '2020-09-03 21:25:29', '2020-09-03 21:25:29'),
(7, 11, 31, 'mohid sultan', 'badar munir asif', 'i\'m fine. how are you?', '2020-09-03 21:25:44', '2020-09-03 21:25:44'),
(8, 33, 27, 'badar asif', 'yousuf javed', 'a', '2020-09-06 10:58:30', '2020-09-06 10:58:30'),
(9, 34, 26, 'badar asif', 'yousuf javed', 'okay', '2020-09-06 10:58:48', '2020-09-06 10:58:48'),
(10, 9, 34, 'yousuf javed', 'saqib  munir', 'see', '2020-09-06 12:51:32', '2020-09-06 12:51:32'),
(11, 34, 26, 'badar asif', 'yousuf javed', 'yes', '2020-09-06 12:51:45', '2020-09-06 12:51:45'),
(12, 9, 34, 'yousuf javed', 'saqib  munir', 'how are you?', '2020-09-06 12:52:00', '2020-09-06 12:52:00'),
(13, 34, 26, 'badar asif', 'yousuf javed', 'i\'m fine and what about you', '2020-09-06 12:52:09', '2020-09-06 12:52:09'),
(14, 9, 34, 'yousuf javed', 'saqib  munir', 'good. thanks', '2020-09-06 12:52:19', '2020-09-06 12:52:19'),
(15, 9, 34, 'yousuf javed', 'saqib  munir', 'what r u doing?', '2020-09-06 12:52:29', '2020-09-06 12:52:29'),
(16, 34, 26, 'badar asif', 'yousuf javed', 'good thanks', '2020-09-06 12:56:06', '2020-09-06 12:56:06'),
(17, 34, 26, 'badar asif', 'yousuf javed', 'okay fine', '2020-09-06 12:56:16', '2020-09-06 12:56:16'),
(18, 34, 26, 'badar asif', 'yousuf javed', 'where are you right now?', '2020-09-06 12:56:27', '2020-09-06 12:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `className` varchar(256) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `addedBy` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `className`, `parent_id`, `addedBy`, `created_at`, `updated_at`) VALUES
(19, 'Science Group', NULL, 23, '2020-08-28 09:15:21', '2020-08-28 09:15:21'),
(20, 'Arts Group', NULL, 23, '2020-08-28 09:15:34', '2020-08-28 09:15:34'),
(21, 'Computer Science', 19, 23, '2020-08-28 09:16:51', '2020-08-28 09:16:51'),
(22, 'Physics', 19, 23, '2020-08-28 09:20:24', '2020-08-28 09:20:24'),
(23, 'Urdu', 20, 23, '2020-08-28 09:22:15', '2020-08-28 09:22:15'),
(24, 'punjabi', 20, 23, '2020-08-28 09:22:29', '2020-08-28 09:22:29'),
(25, 'main demo group', NULL, 23, '2020-08-28 14:40:54', '2020-08-28 14:40:54'),
(26, 'demo sub group', 25, 23, '2020-08-28 14:41:15', '2020-08-28 14:41:15'),
(27, 'testiiiasdf', NULL, 23, '2020-09-01 08:52:58', '2020-09-01 08:52:58'),
(28, 'tesaf', NULL, 23, '2020-09-01 08:53:57', '2020-09-01 08:53:57'),
(31, 'love', NULL, 23, '2020-09-01 09:02:40', '2020-09-01 09:02:40'),
(32, 'love', NULL, 23, '2020-09-01 09:02:40', '2020-09-01 09:02:40'),
(33, 'check', NULL, 23, '2020-09-01 09:03:57', '2020-09-01 09:03:57'),
(34, 'check', NULL, 23, '2020-09-01 09:03:57', '2020-09-01 09:03:57'),
(35, 'love u', NULL, 23, '2020-09-01 09:16:15', '2020-09-01 09:16:15'),
(36, 'yes', NULL, 23, '2020-09-01 09:17:38', '2020-09-01 09:17:38'),
(37, 'yes', NULL, 23, '2020-09-01 09:17:38', '2020-09-01 09:17:38'),
(38, 'love you too', NULL, 23, '2020-09-01 09:17:54', '2020-09-01 09:17:54'),
(39, 'love you too', NULL, 23, '2020-09-01 09:17:54', '2020-09-01 09:17:54'),
(40, '3', NULL, 23, '2020-09-01 09:19:55', '2020-09-01 09:19:55'),
(41, '3', NULL, 23, '2020-09-01 09:19:55', '2020-09-01 09:19:55'),
(42, 'test', NULL, 23, '2020-09-01 09:21:12', '2020-09-01 09:21:12'),
(43, 'test', NULL, 23, '2020-09-01 09:21:12', '2020-09-01 09:21:12'),
(44, 'okay fine', NULL, 23, '2020-09-01 09:22:24', '2020-09-01 09:22:24'),
(45, 'okay fine', NULL, 23, '2020-09-01 09:22:24', '2020-09-01 09:22:24'),
(46, 'ddk', NULL, 23, '2020-09-01 09:23:50', '2020-09-01 09:23:50'),
(47, 'kkd', NULL, 23, '2020-09-01 09:23:58', '2020-09-01 09:23:58'),
(48, 'i love you', NULL, 23, '2020-09-01 09:25:52', '2020-09-01 09:25:52'),
(49, 'love you too', NULL, 23, '2020-09-01 09:26:03', '2020-09-01 09:26:03');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `courseName` varchar(255) NOT NULL,
  `addedBy` tinyint(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `courseName`, `addedBy`, `created_at`, `updated_at`) VALUES
(8, 'Data Structure', 23, '2020-08-28 09:36:42', '2020-08-28 09:36:42'),
(9, 'OOP', 23, '2020-08-28 09:36:52', '2020-08-28 09:36:52'),
(10, 'Algebra', 23, '2020-08-28 09:37:13', '2020-08-28 09:37:13'),
(11, 'statistics', 23, '2020-08-28 09:37:34', '2020-08-28 09:37:34'),
(12, 'poetry', 23, '2020-08-28 09:37:53', '2020-08-28 09:37:53'),
(13, 'ghazal', 23, '2020-08-28 09:38:09', '2020-08-28 09:38:09'),
(14, 'quantum physics', 23, '2020-08-28 09:38:30', '2020-08-28 09:38:30'),
(15, 'bio physics', 23, '2020-08-28 09:38:45', '2020-08-28 09:38:45'),
(16, 'psychology', 23, '2020-08-28 09:39:08', '2020-08-28 09:39:08'),
(17, 'demo course', 23, '2020-08-28 14:42:59', '2020-08-28 14:42:59');

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE `exam` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `courseID` int(11) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `deadline` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`id`, `title`, `description`, `file`, `courseID`, `addedBy`, `deadline`, `created_at`, `updated_at`) VALUES
(2, 'This is exam', 'ddfdf', '', 12, 24, '2020-09-24 00:00:00', '2020-09-09 06:29:00', '2020-09-09 06:29:00');

-- --------------------------------------------------------

--
-- Table structure for table `exam_grade`
--

CREATE TABLE `exam_grade` (
  `id` int(11) NOT NULL,
  `examID` int(11) NOT NULL,
  `studentID` varchar(255) NOT NULL,
  `studentMarks` varchar(255) NOT NULL,
  `examDate` varchar(255) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_grade`
--

INSERT INTO `exam_grade` (`id`, `examID`, `studentID`, `studentMarks`, `examDate`, `addedBy`, `created_at`, `updated_at`) VALUES
(1, 2, '13,14', '5,5', '2020-09-09', 24, '2020-09-09 06:40:41', '2020-09-09 06:40:41');

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `description` longtext NOT NULL,
  `addedBy` int(11) NOT NULL,
  `submitTo` varchar(256) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `subject`, `description`, `addedBy`, `submitTo`, `created_at`, `updated_at`) VALUES
(1, 'instructor are now teaching well', 'instructor with the name (abc) is not teaching well in proper way.', 28, '23', '2020-09-01 08:15:18', '2020-09-01 08:15:18'),
(2, 'instructor are now teaching well', 'okay', 31, '23', '2020-09-02 19:30:46', '2020-09-02 19:30:46'),
(5, 'bc', 'hhggghghhggfdgfd', 36, '23', '2020-09-05 21:24:30', '2020-09-05 21:24:30');

-- --------------------------------------------------------

--
-- Table structure for table `helpdesk`
--

CREATE TABLE `helpdesk` (
  `id` int(11) NOT NULL,
  `sendBy` int(11) NOT NULL,
  `sendTo` int(11) NOT NULL,
  `senderName` varchar(100) NOT NULL,
  `receiverName` varchar(100) NOT NULL,
  `description` longtext,
  `helpID` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `helpdesk`
--

INSERT INTO `helpdesk` (`id`, `sendBy`, `sendTo`, `senderName`, `receiverName`, `description`, `helpID`, `created_at`, `updated_at`) VALUES
(2, 31, 23, 'mohid sultan', 'm munir', 'test', 2, '2020-09-03 07:48:03', '2020-09-03 07:48:03'),
(3, 23, 31, 'mohid sultan', 'm munir', 'etsd', 2, '2020-09-03 07:49:01', '2020-09-03 07:49:01'),
(4, 31, 23, 'mohid sultan', 'm munir', 'asdf', 2, '2020-09-03 07:49:34', '2020-09-03 07:49:34'),
(5, 31, 23, 'mohid sultan', 'm munir', 'asdf', 2, '2020-09-03 07:49:45', '2020-09-03 07:49:45'),
(6, 31, 23, 'mohid sultan', 'm munir', 'asd;flkj', 2, '2020-09-03 07:50:23', '2020-09-03 07:50:23'),
(7, 31, 23, 'mohid sultan', 'm munir', 'jjj', 2, '2020-09-03 07:56:00', '2020-09-03 07:56:00'),
(8, 31, 23, 'mohid sultan', 'm munir', 'asdf', 2, '2020-09-03 08:00:16', '2020-09-03 08:00:16'),
(9, 31, 23, 'mohid sultan', 'm munir', 'sdd', 2, '2020-09-03 08:00:27', '2020-09-03 08:00:27'),
(10, 31, 23, 'mohid sultan', 'm munir', 'sdd', 2, '2020-09-03 08:00:27', '2020-09-03 08:00:27'),
(11, 31, 23, 'mohid sultan', 'm munir', 'dd', 2, '2020-09-03 08:00:40', '2020-09-03 08:00:40'),
(12, 31, 23, 'mohid sultan', 'm munir', 'asdf', 2, '2020-09-03 08:10:32', '2020-09-03 08:10:32'),
(13, 31, 23, 'mohid sultan', 'm munir', 'okay', 2, '2020-09-03 08:10:52', '2020-09-03 08:10:52'),
(14, 31, 23, 'mohid sultan', 'm munir', 'yes', 2, '2020-09-03 08:11:15', '2020-09-03 08:11:15'),
(15, 31, 23, 'mohid sultan', 'm munir', 'and', 2, '2020-09-03 08:12:06', '2020-09-03 08:12:06'),
(16, 31, 23, 'mohid sultan', 'm munir', 'te', 2, '2020-09-03 08:12:29', '2020-09-03 08:12:29'),
(17, 31, 23, 'mohid sultan', 'm munir', 'okay', 2, '2020-09-03 08:12:37', '2020-09-03 08:12:37'),
(18, 31, 23, 'mohid sultan', 'm munir', 'fine', 2, '2020-09-03 08:12:42', '2020-09-03 08:12:42'),
(19, 31, 23, 'mohid sultan', 'm munir', 'yes', 2, '2020-09-03 08:17:12', '2020-09-03 08:17:12'),
(20, 31, 23, 'mohid sultan', 'm munir', 'okay (Y)', 2, '2020-09-03 08:17:22', '2020-09-03 08:17:22'),
(21, 23, 31, 'm munir', 'mohid sultan', 'okay got it', 2, '2020-09-03 08:51:24', '2020-09-03 08:51:24'),
(22, 23, 31, 'm munir', 'mohid sultan', 'sai ha', 2, '2020-09-03 08:52:33', '2020-09-03 08:52:33'),
(23, 23, 31, 'm munir', 'mohid sultan', 'okay', 2, '2020-09-03 08:52:39', '2020-09-03 08:52:39'),
(24, 23, 31, 'm munir', 'mohid sultan', 'hmmm', 2, '2020-09-03 08:52:47', '2020-09-03 08:52:47'),
(25, 31, 23, 'mohid sultan', 'm munir', 'gotcha', 2, '2020-09-03 09:16:43', '2020-09-03 09:16:43'),
(26, 31, 31, 'mohid sultan', 'mohid sultan', 'okay fine', 2, '2020-09-03 12:42:44', '2020-09-03 12:42:44'),
(27, 31, 31, 'mohid sultan', 'mohid sultan', 'yeah', 2, '2020-09-03 12:43:53', '2020-09-03 12:43:53'),
(28, 31, 23, 'mohid sultan', 'm munir', 'yes', 2, '2020-09-03 12:44:30', '2020-09-03 12:44:30'),
(29, 31, 23, 'mohid sultan', 'm munir', 'okay', 2, '2020-09-03 12:45:22', '2020-09-03 12:45:22'),
(30, 31, 23, 'mohid sultan', 'm munir', 'fine', 2, '2020-09-03 12:46:46', '2020-09-03 12:46:46'),
(31, 31, 23, 'mohid sultan', 'm munir', 'lakh di laanat', 2, '2020-09-03 12:46:59', '2020-09-03 12:46:59'),
(32, 23, 31, 'm munir', 'mohid sultan', 'yeah thank you', 2, '2020-09-03 14:51:40', '2020-09-03 14:51:40'),
(33, 31, 23, 'mohid sultan', 'm munir', 'dhur fitty mu', 2, '2020-09-03 14:51:55', '2020-09-03 14:51:55'),
(34, 31, 23, 'mohid sultan', 'm munir', 'i love you', 2, '2020-09-03 14:52:44', '2020-09-03 14:52:44'),
(35, 23, 31, 'm munir', 'mohid sultan', 'i love you too', 2, '2020-09-03 14:52:53', '2020-09-03 14:52:53'),
(42, 36, 23, 'Demo Munir', 'm munir', 'Hello this is instructor', 5, '2020-09-05 21:24:46', '2020-09-05 21:24:46'),
(43, 23, 36, 'm munir', 'Demo Munir', 'This is school admin', 5, '2020-09-05 21:26:22', '2020-09-05 21:26:22'),
(44, 23, 36, 'm munir', 'Demo Munir', 'ok sir', 5, '2020-09-05 21:30:03', '2020-09-05 21:30:03');

-- --------------------------------------------------------

--
-- Table structure for table `instructor`
--

CREATE TABLE `instructor` (
  `id` int(11) NOT NULL,
  `instructorfName` varchar(255) NOT NULL,
  `instructormName` varchar(255) DEFAULT NULL,
  `instructorlName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `instructorPass` varchar(500) NOT NULL,
  `instructorPhone` varchar(255) NOT NULL,
  `instructorAddress` varchar(500) NOT NULL,
  `addedBy` tinyint(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `instructor`
--

INSERT INTO `instructor` (`id`, `instructorfName`, `instructormName`, `instructorlName`, `email`, `instructorPass`, `instructorPhone`, `instructorAddress`, `addedBy`, `created_at`, `updated_at`) VALUES
(9, 'saqib', NULL, 'munir', 'link2saqib@gmail.com', '$2y$10$CswGGZYvb2sbXPLpASbXKOqzFwPZExnZ4IqG/f7P6Htl/cs86aucC', '+23234239423', 'rwp', 23, '2020-08-28 09:12:42', '2020-08-28 09:50:55'),
(10, 'aleem', 'akhtar', 'asif', 'link2aleem@gmail.com', '$2y$10$sBmDh5zXbgsFKD8Sad8dd.0WLi08pPUgUX75TQlcDsykN8hOgntru', '+23942934234', 'jhelum', 23, '2020-08-28 09:13:29', '2020-08-28 10:05:49'),
(11, 'badar', 'munir', 'asif', 'link2badar@gmail.com', '$2y$10$VMZaSpcP69faWbJBv8uBGuP6MJ0lrXpiyZ/jo98ut1pSyzbi8JaDi', '+0329340234', 'jhelum', 23, '2020-08-28 09:14:16', '2020-08-28 10:05:40'),
(12, 'aqib', NULL, 'munir', 'link2aqib@gmail.com', '$2y$10$4n5MhF1mcwrYDeTXYvxBhOxj7jUQmj13A9r4iTs3BgoGSkGY/zUju', '234', 'rwp', 23, '2020-08-28 09:15:04', '2020-08-28 10:05:33'),
(14, 'Demo', 'Badar', 'Munir', 'link2demo@gmail.com', '$2y$10$HS4C750fbDQtSdIzWqDTxe7r81CI.l43rYoybl3Nl/OMVk4fVfbe6', '+423234', 'complete address dummy for the demo', 23, '2020-08-28 14:40:05', '2020-08-28 14:40:05');

-- --------------------------------------------------------

--
-- Table structure for table `lecture`
--

CREATE TABLE `lecture` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext,
  `file` longtext,
  `addedBy` int(11) NOT NULL,
  `courseID` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lecture`
--

INSERT INTO `lecture` (`id`, `title`, `description`, `file`, `addedBy`, `courseID`, `created_at`, `updated_at`) VALUES
(22, 'mirza asad ullah khan ghalib', 'famous poet history', 'mirza asad ullah khan ghalib-128749106220200828095515.zip', 24, 12, '2020-08-28 09:55:16', '2020-08-28 09:55:16'),
(23, 'jaun elia', 'murshad jesa koi ni', NULL, 24, 12, '2020-08-28 09:57:11', '2020-08-28 09:57:11'),
(24, 'stack', 'lifo structure based stack', NULL, 24, 8, '2020-08-28 10:00:51', '2020-08-28 10:00:51'),
(25, 'queue', 'fifo based structure', 'queue-63177736120200828100137.zip', 24, 8, '2020-08-28 10:01:37', '2020-08-28 10:01:37'),
(26, 'lecture 1', 'demo for the lecture 1', 'lecture 1-37554377820200828144947.zip', 36, 17, '2020-08-28 14:49:47', '2020-08-28 14:49:47'),
(27, 'lecture 1', 'ksdk', NULL, 26, 10, '2020-09-01 09:45:40', '2020-09-01 09:45:40');

-- --------------------------------------------------------

--
-- Table structure for table `lecture_grade`
--

CREATE TABLE `lecture_grade` (
  `id` int(11) NOT NULL,
  `lectureID` int(11) NOT NULL,
  `studentID` varchar(256) NOT NULL,
  `studentMarks` varchar(256) NOT NULL,
  `lectureDate` varchar(100) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `lecture_grade`
--

INSERT INTO `lecture_grade` (`id`, `lectureID`, `studentID`, `studentMarks`, `lectureDate`, `addedBy`, `created_at`, `updated_at`) VALUES
(9, 25, '8,9', '10,8', '28/Aug/2020', 24, '2020-08-28 10:09:26', '2020-08-28 10:09:26'),
(10, 24, '8,9', '9,12', '28/Aug/2020', 24, '2020-08-28 10:20:40', '2020-08-28 10:20:40'),
(11, 26, '15', '22', '28/Aug/2020', 36, '2020-08-28 14:52:29', '2020-08-28 14:52:29');

-- --------------------------------------------------------

--
-- Table structure for table `material`
--

CREATE TABLE `material` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext NOT NULL,
  `file` longtext NOT NULL,
  `addedBy` tinyint(10) NOT NULL,
  `courseID` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material`
--

INSERT INTO `material` (`id`, `title`, `description`, `file`, `addedBy`, `courseID`, `created_at`, `updated_at`) VALUES
(8, 'linked list', 'we will discuss in the upcoming monday.', '', 24, 8, '2020-08-28 11:10:46', '2020-08-28 11:10:46');

-- --------------------------------------------------------

--
-- Table structure for table `material_grade`
--

CREATE TABLE `material_grade` (
  `id` int(11) NOT NULL,
  `materialID` int(11) NOT NULL,
  `studentID` varchar(255) NOT NULL,
  `studentMarks` varchar(255) NOT NULL,
  `materialDate` varchar(100) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `material_grade`
--

INSERT INTO `material_grade` (`id`, `materialID`, `studentID`, `studentMarks`, `materialDate`, `addedBy`, `created_at`, `updated_at`) VALUES
(3, 8, '8,9', '3,4', '28/Aug/2020', 24, '2020-08-28 11:11:46', '2020-08-28 11:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` longtext,
  `file` varchar(256) DEFAULT NULL,
  `courseID` int(11) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `deadline` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz`
--

INSERT INTO `quiz` (`id`, `title`, `description`, `file`, `courseID`, `addedBy`, `deadline`, `created_at`, `updated_at`) VALUES
(1, 'test', 'testing', NULL, 17, 26, '2020-09-06 03:00:00', '2020-09-06 14:09:57', '2020-09-06 14:09:57'),
(2, 'testing', 'asdf', NULL, 17, 26, '2020-09-07 03:00:00', '2020-09-06 14:21:23', '2020-09-06 14:21:23'),
(3, 'This is quiz', 'dff', NULL, 12, 24, '0000-00-00 00:00:00', '2020-09-09 06:30:10', '2020-09-09 06:30:10');

-- --------------------------------------------------------

--
-- Table structure for table `quiz_grade`
--

CREATE TABLE `quiz_grade` (
  `id` int(11) NOT NULL,
  `quizID` int(11) NOT NULL,
  `studentID` varchar(256) NOT NULL,
  `studentMarks` varchar(256) NOT NULL,
  `quizDate` datetime NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `quiz_grade`
--

INSERT INTO `quiz_grade` (`id`, `quizID`, `studentID`, `studentMarks`, `quizDate`, `addedBy`, `created_at`, `updated_at`) VALUES
(4, 1, '13,14', '10,3', '1970-01-01 00:00:00', 26, '2020-09-06 20:51:00', '2020-09-06 20:51:00'),
(5, 2, '13,14', '03,33', '2020-06-09 00:00:00', 26, '2020-09-06 20:54:48', '2020-09-06 20:54:48'),
(6, 3, '13,14', '4,6', '2020-09-09 00:00:00', 24, '2020-09-09 06:44:20', '2020-09-09 06:44:20');

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `schoolName` varchar(256) NOT NULL,
  `adminName` varchar(100) NOT NULL,
  `email` varchar(256) NOT NULL,
  `schoolPass` varchar(256) NOT NULL,
  `schoolPhone` varchar(100) DEFAULT NULL,
  `schoolAddress` varchar(256) DEFAULT NULL,
  `totalClasses` int(11) NOT NULL,
  `totalStudents` int(11) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`id`, `schoolName`, `adminName`, `email`, `schoolPass`, `schoolPhone`, `schoolAddress`, `totalClasses`, `totalStudents`, `addedBy`, `created_at`, `updated_at`) VALUES
(10, 'The Lyceum school system', 'm munir', 'link2munir@gmail.com', '123456', '+923447575573', 'pdkhan', 10, 1000, 1, '2020-08-28 09:08:31', '2020-08-28 09:08:31');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `studentfName` varchar(255) NOT NULL,
  `studentmName` varchar(255) DEFAULT NULL,
  `studentlName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `studentPass` varchar(500) NOT NULL,
  `studentAddress` varchar(500) NOT NULL,
  `class_id` int(11) NOT NULL,
  `addedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `studentPhone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `studentfName`, `studentmName`, `studentlName`, `email`, `studentPass`, `studentAddress`, `class_id`, `addedBy`, `created_at`, `updated_at`, `studentPhone`) VALUES
(9, 'javeria', NULL, 'khan', 'link2javeria@gmail.com', '$2y$10$VB6I4jA2dTSXPVhn./MGR.XLhq4BRSGg/LiOlw5EMkUtwS/0nxzC2', 'mandi', 21, 23, '2020-08-28 09:26:07', '2020-08-28 10:05:18', 234),
(10, 'zimal', NULL, 'fatima', 'link2zimal@gmail.com', '$2y$10$XKNwjvf5w3v.gLn6eRBl.eB3fGbA7IModrZf7bn93nxE4JGdU5uOu', 'pdkhan', 22, 23, '2020-08-28 09:28:59', '2020-08-28 10:05:09', 23482323),
(11, 'mohid', 'aleem', 'sultan', 'link2mohid@gmail.com', '$2y$10$ay35rg9682CXuMHsoB/LX.9QQeVTiP7QrNGP2EMXUrteaXbp3B32i', 'isb', 23, 23, '2020-08-28 09:30:48', '2020-08-28 10:05:01', 234823333),
(12, 'm', NULL, 'hussnain', 'link2hussnain@gmail.com', '$2y$10$utGnWvEZ3hwM8OFAPUkEWus2aaUxuhbzUEAle9CaQlvZKdnYa3GQy', 'isb', 23, 23, '2020-08-28 09:33:01', '2020-08-28 10:04:50', 2147483647),
(13, 'aiman', NULL, 'javed', 'link2aiman@gmail.com', '$2y$10$Ucvt6hAnvq9gXORZHdW8ueBPBFv476/UUpngJRB7dW8hJYmN.wREe', 'mkw', 24, 23, '2020-08-28 09:35:06', '2020-08-28 10:04:39', 2147483647),
(14, 'yousuf', 'k', 'javed', 'link2yousuf@gmail.com', '$2y$10$Rfi2El9VJsZilztg8OLzTes9DrTWkoVtB9r8sFcz5DXipYSlDCOqe', 'mkw', 24, 23, '2020-08-28 09:35:46', '2020-09-01 12:02:52', 2147483647);

-- --------------------------------------------------------

--
-- Table structure for table `submit_assignment`
--

CREATE TABLE `submit_assignment` (
  `id` int(11) NOT NULL,
  `description` longtext,
  `file` varchar(256) DEFAULT NULL,
  `assignmentID` int(11) NOT NULL,
  `submittedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `submit_assignment`
--

INSERT INTO `submit_assignment` (`id`, `description`, `file`, `assignmentID`, `submittedBy`, `created_at`, `updated_at`) VALUES
(7, 'submission of calculator by using stack.', '149801412320200828110312.zip', 12, 28, '2020-09-01 05:03:12', '2020-08-28 11:03:12'),
(8, 'dummy description for the demo', '202233553120200828151034.zip', 13, 37, '2020-09-03 15:10:35', '2020-08-28 15:10:35'),
(9, 'jjk', NULL, 16, 28, '2020-08-29 05:06:51', '2020-08-29 05:06:51'),
(10, 'okay', NULL, 19, 28, '2020-09-01 09:44:37', '2020-09-01 09:44:37'),
(11, 'estt', NULL, 18, 28, '2020-09-01 12:41:51', '2020-09-01 12:41:51');

-- --------------------------------------------------------

--
-- Table structure for table `submit_exam`
--

CREATE TABLE `submit_exam` (
  `id` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `file` varchar(255) DEFAULT NULL,
  `examID` int(11) NOT NULL,
  `submittedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `submit_exam`
--

INSERT INTO `submit_exam` (`id`, `description`, `file`, `examID`, `submittedBy`, `created_at`, `updated_at`) VALUES
(1, 'This student submit exam', NULL, 2, 33, '2020-09-09 07:17:43', '2020-09-09 07:17:43');

-- --------------------------------------------------------

--
-- Table structure for table `submit_quiz`
--

CREATE TABLE `submit_quiz` (
  `id` int(11) NOT NULL,
  `description` longtext,
  `file` varchar(256) DEFAULT NULL,
  `quizID` int(11) NOT NULL,
  `submittedBy` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `submit_quiz`
--

INSERT INTO `submit_quiz` (`id`, `description`, `file`, `quizID`, `submittedBy`, `created_at`, `updated_at`) VALUES
(1, 'test', NULL, 2, 34, '2020-09-07 10:12:05', '2020-09-07 10:12:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Super Admin', 'superadmin@gmail.com', NULL, '$2y$10$Zg98qzOGdddZkwmnBn2IXuxtjrld1gBNdlx9BhNxwDJL.c7X1Wzxq', 'WS9Ymlp51LTuuWJNQpXjbX804Wi0p1m4JMFUvV8fubtzwt5xJlxItdeIjKQt', '2020-08-07 10:32:30', '2020-08-17 09:51:48', 0),
(23, 'm munir', 'link2munir@gmail.com', NULL, '$2y$10$haBLojLnfMlNB4mkI8Oj..g.Chd3.hn1DKvOCrlWOa4QyysL8JKhK', NULL, '2020-08-28 04:08:31', '2020-08-28 04:08:31', 1),
(24, 'saqib munir', 'link2saqib@gmail.com', NULL, '$2y$10$0ziPdt.fhOTpzDcB9WNmOeytSfT9k47SKiYBDWItnbb68jJi9VyLm', NULL, '2020-08-28 04:12:42', '2020-08-28 04:50:55', 2),
(25, 'aleem asif', 'link2aleem@gmail.com', NULL, '$2y$10$qpXfaGOTlAI3mRjjJPZNrOQK4cRHuy0iykpej5W7lxwx2tW.rGFMC', NULL, '2020-08-28 04:13:30', '2020-08-28 05:05:49', 2),
(26, 'badar asif', 'link2badar@gmail.com', NULL, '$2y$10$4bubeZNIMvWUe22c2GhxJeKKpuWEZTEOvqPloPEWRd2qwGLSM.fd2', NULL, '2020-08-28 04:14:17', '2020-08-28 05:05:40', 2),
(27, 'aqib munir', 'link2aqib@gmail.com', NULL, '$2y$10$/FXLds.Oki8cftFLlB2D1u.bRRn0pMUF3ggsTDOXnI7KJkip0wYiu', NULL, '2020-08-28 04:15:04', '2020-08-28 05:05:33', 2),
(29, 'javeria khan', 'link2javeria@gmail.com', NULL, '$2y$10$SqmRRkS.8QB.6nEEo2bG7uUv5b7MF0qbu3h0NXLs4z1mn3FTi4SDi', NULL, '2020-08-28 04:26:07', '2020-08-28 05:05:18', 3),
(30, 'zimal fatima', 'link2zimal@gmail.com', NULL, '$2y$10$YvvvNQofMKazqa4kywqJce4RO53lIiCt.201HVpOb6AE422pf//fu', NULL, '2020-08-28 04:28:59', '2020-08-28 05:05:09', 3),
(31, 'mohid sultan', 'link2mohid@gmail.com', NULL, '$2y$10$lEhkMmjZ.3AAdfeCQ3/rCuCgbCh/BCTGGsR1vibnfbTFxMPwfkn82', NULL, '2020-08-28 04:30:48', '2020-08-28 05:05:01', 3),
(32, 'm hussnain', 'link2hussnain@gmail.com', NULL, '$2y$10$DoSlLJ9VIO568Q2jwuwJoOBOmX5c4vnu6PDTuAeO5iC3exKzFdrly', NULL, '2020-08-28 04:33:01', '2020-08-28 05:04:50', 3),
(33, 'aiman javed', 'link2aiman@gmail.com', NULL, '$2y$10$bIJmdgs7NH.JUU6FTo88Be/aK5g934U79dhUGE8nwPnALJPPtjzxi', NULL, '2020-08-28 04:35:07', '2020-08-28 05:04:40', 3),
(34, 'yousuf javed', 'link2yousuf@gmail.com', NULL, '$2y$10$SOsINDN0StPqzzzT/jTgXe6Kmo5OxZYAHC2CaaLLrpgkbkq1t/fUe', NULL, '2020-08-28 04:35:46', '2020-08-28 05:04:31', 3),
(36, 'Demo Munir', 'link2demo@gmail.com', NULL, '$2y$10$bOOUUgEbubOD6feFqu0I1uQr3U6R3N8x8xWMsDDUfSPWn0vzspuQa', NULL, '2020-08-28 09:40:05', '2020-08-28 09:40:05', 2);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` longtext NOT NULL,
  `addedBy` int(11) NOT NULL,
  `courseID` int(11) NOT NULL,
  `description` longtext,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `title`, `file`, `addedBy`, `courseID`, `description`, `created_at`, `updated_at`) VALUES
(3, 'This is title', '174341151820200905230702.mp4', 24, 12, 'hhhg', '2020-09-05 23:25:15', '2020-09-05 23:25:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `assign`
--
ALTER TABLE `assign`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment`
--
ALTER TABLE `assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment_grade`
--
ALTER TABLE `assignment_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam`
--
ALTER TABLE `exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_grade`
--
ALTER TABLE `exam_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `helpdesk`
--
ALTER TABLE `helpdesk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instructor`
--
ALTER TABLE `instructor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture`
--
ALTER TABLE `lecture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lecture_grade`
--
ALTER TABLE `lecture_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material`
--
ALTER TABLE `material`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `material_grade`
--
ALTER TABLE `material_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_grade`
--
ALTER TABLE `quiz_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submit_assignment`
--
ALTER TABLE `submit_assignment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submit_exam`
--
ALTER TABLE `submit_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `submit_quiz`
--
ALTER TABLE `submit_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `assign`
--
ALTER TABLE `assign`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `assignment`
--
ALTER TABLE `assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `assignment_grade`
--
ALTER TABLE `assignment_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `exam`
--
ALTER TABLE `exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_grade`
--
ALTER TABLE `exam_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `helpdesk`
--
ALTER TABLE `helpdesk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `instructor`
--
ALTER TABLE `instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `lecture`
--
ALTER TABLE `lecture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `lecture_grade`
--
ALTER TABLE `lecture_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `material`
--
ALTER TABLE `material`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `material_grade`
--
ALTER TABLE `material_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz`
--
ALTER TABLE `quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `quiz_grade`
--
ALTER TABLE `quiz_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `submit_assignment`
--
ALTER TABLE `submit_assignment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `submit_exam`
--
ALTER TABLE `submit_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `submit_quiz`
--
ALTER TABLE `submit_quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
