
<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

//---------------Dashboard---------------------
Route::get('/dashboard', 'DashboardController@index');
Route::any('/profile', 'DashboardController@editProfile');
Route::any('/update-profile', 'DashboardController@updateProfile');
Route::any('/update-instructor-profile', 'DashboardController@updateInstructorProfile');
Route::any('/update-student-profile', 'DashboardController@updateStudentProfile');

//---------------Schools---------------------
Route::get('/all-schools', 'SchoolController@index');
Route::any('/register-new-school', 'SchoolController@add');
Route::any('/save-new-school', 'SchoolController@save');
Route::any('/delete-school/{id}', 'SchoolController@delete');
Route::any('/edit-school/{id}', 'SchoolController@edit');
Route::any('/update-school/{id}', 'SchoolController@update');
Route::any('/school/detail/{id}', 'SchoolController@detail');

//---------------Classes---------------------
Route::any('/all-groups', 'ClassController@index');
Route::any('/register-new-class', 'ClassController@add');
Route::any('/save-new-class', 'ClassController@save');
Route::any('/delete-class/{id}', 'ClassController@delete');
Route::any('/edit-class/{id}', 'ClassController@edit');
Route::any('/update-class/{id}', 'ClassController@update');
Route::any('/class/detail/{id}', 'ClassController@detail');
Route::any('/add-subclass/{id}', 'ClassController@addSub');


//---------------Courses created---------------------
Route::get('/all-courses', 'CourseController@index');
Route::any('/register-new-course', 'CourseController@add');
Route::any('/save-new-course', 'CourseController@save');
Route::any('/delete-course/{id}', 'CourseController@delete');
Route::any('/edit-course/{id}', 'CourseController@edit');
Route::any('/update-course/{id}', 'CourseController@update');
Route::any('/course/detail/{id}', 'CourseController@detail');


//---------------Instructors created---------------------
Route::get('/all-instructors', 'InstructorController@index');
Route::any('/register-new-instructor', 'InstructorController@add');
Route::any('/save-new-instructor', 'InstructorController@save');
Route::any('/delete-instructor/{id}', 'InstructorController@delete');
Route::any('/edit-instructor/{id}', 'InstructorController@edit');
Route::any('/update-instructor/{id}', 'InstructorController@update');
Route::any('/instructor/detail/{id}', 'InstructorController@detail');

//---------------Students created---------------------
Route::get('/all-students', 'StudentController@index');
Route::any('/register-new-student/{id}', 'StudentController@add');
Route::any('/save-new-student', 'StudentController@save');
Route::any('/delete-student/{id}', 'StudentController@delete');
Route::any('/edit-student/{id}', 'StudentController@edit');
Route::any('/update-student/{id}', 'StudentController@update');
Route::any('/student/detail/{id}', 'StudentController@detail');

//--------------Assign instructors---------------------
Route::get('all-assign', 'AssignController@index');
Route::any('/assign-new-instructor', 'AssignController@add');
Route::any('/save-assign', 'AssignController@saveAssign');
Route::any('/edit-assign/{id}', 'AssignController@edit');
Route::any('/update-assign/{id}', 'AssignController@updateAssign');
Route::any('/delete-assign/{id}', 'AssignController@delete');

//--------------- Grading -----------------------------------------
Route::any('/school-lecture-grade', 'LectureController@schoolLectureGrade');
Route::any('/school-assignment-grade', 'AssignmentController@schoolAssignmentGrade');
Route::any('/school-material-grade', 'MaterialController@schoolMaterialGrade');


//------------------ instructor content --------------------------------
Route::any('/school-lectures', 'LectureController@schoolindex');
Route::any('/school-assignments', 'AssignmentController@schoolindex');
Route::any('/school-material', 'MaterialController@schoolindex');


//------------------ Student Help  --------------------------------
Route::any('/school-help', 'HelpController@schoolHelp');
Route::any('/school-help-detail/{id}', 'HelpController@schoolHelpDetail');
Route::any('/school-save-help-detail', 'HelpController@schoolSaveHelpDetail');


//------------------ Instuctor Help  --------------------------------
Route::any('/school-instuctor-help', 'HelpController@schooliHelp');
Route::any('/school-instuctor-help-detail/{id}', 'HelpController@schooliHelpDetail');


Route::any('/school-help-intervalChat/{id}', 'HelpController@schoolHelpIntervalChat');




//############### Instructor Dashboard ###################
//---------------Assigned Courses-----------------------
Route::any('/all-assigned-courses', 'AssignController@getAssignedCourses');

//----------------- Lectures ------------------------
Route::any('/upload-lecture/{id}', 'LectureController@uploadLecture');
Route::any('/save-lecture', 'LectureController@saveLecture');
Route::any('/all-lectures', 'LectureController@index');
Route::any('/edit-lecture/{id}', 'LectureController@edit');
Route::any('/delete-lecture/{id}', 'LectureController@delete');
Route::any('/update-lecture/{id}', 'LectureController@update');
Route::any('/download-lecture/{id}', 'LectureController@download');



//----------------- Video Lectures ------------------------
Route::any('/upload-video/{id}', 'VideoController@uploadVideo');
Route::any('/save-video', 'VideoController@savevideo');
Route::any('/all-videos', 'VideoController@index');
Route::any('/edit-video/{id}', 'VideoController@edit');
Route::any('/delete-video/{id}', 'VideoController@delete');
Route::any('/update-video/{id}', 'VideoController@update');
Route::any('/watch-video/{id}', 'VideoController@watch');


//----------------- Assignments ------------------------
Route::any('/upload-assignment/{id}', 'AssignmentController@uploadAssignment');
Route::any('/save-assignment', 'AssignmentController@saveAssignment');
Route::any('/all-assignments', 'AssignmentController@index');
Route::any('/edit-assignment/{id}', 'AssignmentController@edit');
Route::any('/delete-assignment/{id}', 'AssignmentController@delete');
Route::any('/update-assignment/{id}', 'AssignmentController@update');
Route::any('/download-assignment/{id}', 'AssignmentController@download');

//---------------- Quizzes -------------------------------------
Route::any('/upload-quiz/{id}', 'QuizController@uploadQuiz');
Route::any('/save-quiz', 'QuizController@saveQuiz');
Route::any('/all-quizzes', 'QuizController@index');
Route::any('/download-quiz/{id}', 'QuizController@download');
Route::any('/submission-quiz-list/{id}', 'QuizController@submissionList');


//---------------- Exams -------------------------------------
Route::any('/upload-exam/{id}', 'ExamController@uploadExam');
Route::any('/save-exam', 'ExamController@saveExam');
Route::any('/all-exams', 'ExamController@index');
Route::any('/download-exam/{id}', 'ExamController@download');
Route::any('/submission-exam-list/{id}', 'ExamController@submissionList');


//----------------- Practice Material ------------------------
Route::any('/upload-material/{id}', 'MaterialController@uploadMaterial');
Route::any('/save-material', 'MaterialController@saveMaterial');
Route::any('/all-material', 'MaterialController@index');
Route::any('/edit-material/{id}', 'MaterialController@edit');
Route::any('/delete-material/{id}', 'MaterialController@delete');
Route::any('/update-material/{id}', 'MaterialController@update');
Route::any('/download-material/{id}', 'MaterialController@download');

//--------------- Grading -----------------------------------------
//lecture
Route::any('/lecture-grade/{id}', 'LectureController@lectureGrade');
Route::any('/save-lecture-grade', 'LectureController@saveLectureGrade');
Route::any('/all-lecture-grade', 'LectureController@allLectureGrade');
//assignment
Route::any('/assignment-grade/{id}', 'AssignmentController@assignmentGrade');
Route::any('/save-assignment-grade', 'AssignmentController@saveAssignmentGrade');
Route::any('/all-assignment-grade', 'AssignmentController@allAssignmentGrade');
//material
Route::any('/material-grade/{id}', 'MaterialController@materialGrade');
Route::any('/save-material-grade', 'MaterialController@saveMaterialGrade');
Route::any('/all-material-grade', 'MaterialController@allMaterialGrade');
//Quiz
Route::any('/quiz-grade/{id}', 'QuizController@quizGrade');
Route::any('/save-quiz-grade', 'QuizController@saveQuizGrade');
Route::any('/instructor-quiz-grade', 'QuizController@allQuizGrade');


//Exam
Route::any('/exam-grade/{id}', 'ExamController@examGrade');
Route::any('/save-exam-grade', 'ExamController@saveExamGrade');
Route::any('/instructor-exam-grade', 'ExamController@allExamGrade');

//------------------------- chat ------------------------------------------
Route::any('/instructor-chat/{id?}', 'ChatController@instructorChat');
Route::any('/instructor-save-chat-detail', 'ChatController@instructorSaveChatDetail');
Route::any('/instructor-chat-intervalChat/{id}', 'ChatController@instructorChatIntervalChat');


//############### Student Dashboard ###################
//---------------Assigned Courses, lectures, materials, assignments, quizzes-----------------------
Route::any('/all-enrolled-courses', 'AssignController@getEnrolledCourses');
Route::any('/all-student-lectures/{id}', 'LectureController@getStudentLectures');
Route::any('/all-student-practices/{id}', 'MaterialController@getStudentPractices');
Route::any('/all-student-assignments/{id}', 'AssignmentController@getStudentAssignments');  
Route::any('/all-student-video/{id}', 'VideoController@getStudentVideos');  
Route::any('/assignment-submit/{id}', 'AssignmentController@assignmentSubmit');
Route::any('/submit-assignment-by-student', 'AssignmentController@submitAssignmentByStudent');
Route::any('/submission-list/{id}', 'AssignmentController@submissionList');
Route::any('/all-student-quizzes/{id}', 'QuizController@getStudentQuizzes');  
Route::any('/quiz-submit/{id}', 'QuizController@quizSubmit');
Route::any('/submit-quiz-by-student', 'QuizController@submitQuizByStudent');
Route::any('/all-student-exams/{id}', 'ExamController@getStudentExams');  
Route::any('/exam-submit/{id}', 'ExamController@examSubmit');
Route::any('/submit-exam-by-student', 'ExamController@submitExamByStudent');



//--------------- Grading -----------------------------------------
Route::any('/student-lecture-grade', 'LectureController@studentLectureGrade');
Route::any('/student-assignment-grade', 'AssignmentController@studentAssignmentGrade');
Route::any('/student-material-grade', 'MaterialController@studentMaterialGrade');
Route::any('/student-quiz-grade', 'QuizController@studentQuizGrade');
Route::any('/student-exam-grade', 'ExamController@studentExamGrade');

//------------------ Student help -----------------------------------------------
Route::any('/student-help', 'HelpController@studentHelp');
Route::any('/submit-new-help', 'HelpController@registerStudentHelp');
Route::any('/submit-student-help', 'HelpController@submitStudentHelp');
Route::any('/help-detail/{id}', 'HelpController@helpDetail');
Route::any('/save-help-detail', 'HelpController@saveHelpDetail');

//------------------ Instructor help -----------------------------------------------
Route::any('/instructor-help', 'HelpController@instructorHelp');
Route::any('/submit-new-ihelp', 'HelpController@registerInstructorHelp');
Route::any('/submit-instructor-help', 'HelpController@submitInstructorHelp');
Route::any('/ihelp-detail/{id}', 'HelpController@ihelpDetail');
Route::any('/save-ihelp-detail', 'HelpController@saveiHelpDetail');




Route::any('/help-intervalChat/{id}', 'HelpController@helpIntervalChat');

//------------------------- chat ------------------------------------------
Route::any('/student-chat/{id?}', 'ChatController@studentChat');
Route::any('/save-chat-detail', 'ChatController@saveChatDetail');
Route::any('/chat-intervalChat/{id}', 'ChatController@chatIntervalChat');



